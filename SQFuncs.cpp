#include "SQFuncs.h"
#include "SQMain.h"
#include <stdio.h>
#include "vIRC/Source/IRC.h"
#include "VCMP.h"

extern PluginFuncs * gFuncs;
extern HSQUIRRELVM v;
extern HSQAPI sq;

IRCLinkedList* head = NULL;
struct IRCLinkedList {
	CIRC* pIRC;
	IRCLinkedList* next;
};

bool g_bRunning = false;

void IRCMessage( const char * format, ... )
{
#ifdef WIN32
	HANDLE hstdout = GetStdHandle( STD_OUTPUT_HANDLE );

	CONSOLE_SCREEN_BUFFER_INFO csbBefore;
	GetConsoleScreenBufferInfo( hstdout, &csbBefore );
	SetConsoleTextAttribute( hstdout, FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN );
	printf( "[IRC]  " );

	SetConsoleTextAttribute( hstdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY );
	va_list arg;
	int done;

	va_start (arg, format);
	done = vfprintf (stdout, format, arg);
	va_end (arg);
	printf( "\n" );

	SetConsoleTextAttribute( hstdout, csbBefore.wAttributes );
#else
	printf( "%c[0;33m[IRC]%c[0;37m ", 27, 27 );
	va_list arg;
	int done;
	va_start (arg, format);
	done = vfprintf (stdout, format, arg);
	va_end (arg);
	printf( "\n" );
#endif
}

int __stdcall RawOutput( const char* szHost, const char* szData, CIRC* pIRC )
{
	//printf( "Host: %s, Data: %s\n", szHost, szData );
	int top = sq->gettop(v);
	sq->pushroottable(v);
	sq->pushstring(v,_SC("IRC_onRaw"),-1);
	if(SQ_SUCCEEDED(sq->get(v,-2))) {
		sq->pushroottable(v);
		sq->pushuserpointer(v, pIRC);
		sq->pushstring(v, szHost, -1);
		sq->pushstring(v, szData, -1);
		sq->call(v,4,0,1);
	}
	sq->settop(v, top);
	return 1;
}

int __stdcall PrivMsg( const char* szHost, const char* szData, CIRC* pIRC )
{
	CIRCServerInfo*	pServerInfo = pIRC->GetServerInfo();

	char szTmp[ 512 ] = { 0 };
	strcpy( szTmp, szHost );
	char* szNick = (char*)alloca( pServerInfo->GetMaxNickLen() );
	irc_strcpy( szNick, strtok( szTmp, "!" ) + 1, pServerInfo->GetMaxNickLen() );

	strcpy( szTmp, szData );
	char* szChannel = (char*)alloca( pServerInfo->GetMaxChanNameLen() );
	irc_strcpy( szChannel, strtok( szTmp, " " ), pServerInfo->GetMaxChanNameLen() );

	// Max message length is 512
	char szMessage[ 510 ] = { 0 };
	irc_strcpy( szMessage, szData + strlen( szChannel ) + 1 + 1, sizeof( szMessage ) );

	CIRCChannel* pChan = pIRC->GetChannels()->Find( szChannel );
	const char* szModes = { 0 };
	if ( pChan )
	{
		szModes = pChan->GetUserModes( szNick );
	}
	
	if ( szMessage[0] != 0x1 ) {
		int top = sq->gettop(v);
		sq->pushroottable(v);
		sq->pushstring(v,_SC("IRC_onPrivMsg"),-1);
		if(SQ_SUCCEEDED(sq->get(v,-2))) {
			sq->pushroottable(v);
			sq->pushuserpointer(v, pIRC);
			sq->pushstring(v, szHost, -1);
			sq->pushstring(v, szChannel, -1);
			sq->pushstring(v, szMessage, -1);
			//sq->pushstring(v, szModes, -1);
			sq->call(v,5,0,1);
		}
		sq->settop(v, top);
	}

	return 1;
}

int __stdcall AllRPLNumerics( unsigned int uiNumeric, const char* szHost, const char* szData, CIRC* pIRC )
{
	int top = sq->gettop(v);
	sq->pushroottable(v);
	sq->pushstring(v, _SC("IRC_onNumeric"), -1);
	if (SQ_SUCCEEDED(sq->get(v, -2))) {
		sq->pushroottable(v);
		sq->pushuserpointer(v, pIRC);
		sq->pushinteger(v, uiNumeric);
		sq->pushstring(v, szHost, -1);
		sq->pushstring(v, szData, -1);
		sq->call(v, 5, 0, 1);
	}
	sq->settop(v, top);
	return 1;
}

int __stdcall Connected( const char* szHost, const char* szData, CIRC* pIRC )
{
	IRCMessage( "Connected!" );
	int top = sq->gettop(v);
	sq->pushroottable(v);
	sq->pushstring(v,_SC("IRC_onConnected"),-1);
	if(SQ_SUCCEEDED(sq->get(v,-2))) {
		sq->pushroottable(v);
		sq->pushuserpointer(v, pIRC);
		sq->call(v,2,0,1);
	}
	sq->settop(v, top);
	return 1;
}

int __stdcall Disconnected( CIRC* pIRC )
{
	IRCMessage( "Disconnected from IRC network." );

	IRCMessage( "Attempting to reconnect..." );
	if ( pIRC->Connect() == IRC_CONN_SUCCESSFUL )
	{
		IRCMessage( "Connected!" );
	}
	else 
		IRCMessage( "Unable to reconnect." );

	return 1;
}

_SQUIRRELDEF( IRC_Init )
{
	CIRC* pIRC = CIRCManager::New();
	IRCLinkedList* node = (IRCLinkedList*)malloc(sizeof(IRCLinkedList));

	if (head == NULL)
	{
		head = node;
		head->pIRC = pIRC;
		head->next = NULL;
	}
	else
	{
		node->pIRC = pIRC;
		node->next = NULL;

		IRCLinkedList* newHead = head;
		while (newHead != NULL)
		{
			if (newHead->next == NULL)
			{
				newHead->next = node;
				break;
			}

			newHead = newHead->next;
		}
	}

	sq->pushuserpointer(v, pIRC);
	return 1;
}

_SQUIRRELDEF( IRC_Disconnect )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount == 2)
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		if (pIRC != NULL)
			CIRCManager::Remove(pIRC);

		IRCLinkedList* node = head;
		IRCLinkedList* prev = NULL;
		IRCLinkedList* res = NULL;

		while (node != NULL)
		{
			if (node->pIRC == pIRC)
			{
				if (node == head)
				{
					res = head;
					head = head->next;
				}
				else
				{
					prev->next = node->next;
					res = node;
				}

				break;
			}

			prev = node;
			node = node->next;
		}

		if (res != NULL) free(res);
	}

	return 0;
}

_SQUIRRELDEF( IRC_SetIP )
{
	SQInteger iArgCount = sq->gettop(v);
	if( iArgCount == 3 )
	{
		CIRC* pIRC;
		const SQChar* szIP;

		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);
		sq->getstring( v, 3, &szIP );

		if (pIRC)
			pIRC->SetIP((char*)szIP);
	}

	return 0;
}
_SQUIRRELDEF( IRC_SetPort )
{
	SQInteger iArgCount = sq->gettop(v);
	if( iArgCount == 3 )
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		SQInteger port = 6667;
		sq->getinteger( v, 3, &port );
		
		if (pIRC)
			pIRC->SetPort(port);
	}

	return 0;
}
_SQUIRRELDEF( IRC_SetNick )
{
	SQInteger iArgCount = sq->gettop(v);
	if( iArgCount == 3 )
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		const SQChar *szNick;
		sq->getstring( v, 3, &szNick );
			
		if (pIRC)
			pIRC->SetNick(szNick);
	}

	return 0;
}
_SQUIRRELDEF( IRC_SetUser )
{
	SQInteger iArgCount = sq->gettop(v);
	if( iArgCount == 3 )
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		const SQChar *szUser;
		sq->getstring( v, 3, &szUser );

		if (pIRC)
			pIRC->SetUsername(szUser);
	}

	return 0;
}
_SQUIRRELDEF( IRC_SetRealname )
{
	SQInteger iArgCount = sq->gettop(v);
	if( iArgCount == 3 )
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		const SQChar *szRealname;
		sq->getstring( v, 3, &szRealname );

		if (pIRC)
			pIRC->SetRealname(szRealname);
	}

	return 0;
}
_SQUIRRELDEF( IRC_SetPass )
{
	SQInteger iArgCount = sq->gettop(v);
	if( iArgCount == 3 )
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		const SQChar *szPass;
		sq->getstring( v, 3, &szPass );

		if (pIRC)
			pIRC->SetPass(szPass);
	}

	return 0;
}

_SQUIRRELDEF( IRC_Connect )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 2)
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		SQBool usingSSL = 0;
		if (iArgCount >= 3)
			sq->getbool(v, 3, &usingSSL);

		pIRC->SetUsingSSL(usingSSL);
		pIRC->SetVersionReply("SqIRC Module by Thijn");

		pIRC->SetRawCallback((void*)RawOutput);
		pIRC->SetDisconnectCallback((void*)Disconnected);
		pIRC->AddIRCCmdHook("PRIVMSG", (void*)PrivMsg);
		pIRC->AddNumericIRCCmdHook(RPL_WELCOME, (void*)Connected);
		pIRC->AddNumericIRCCmdHook(RPL_ALL, (void*)AllRPLNumerics);

		IRCMessage("Connecting...");
		if (pIRC && pIRC->Connect() == IRC_CONN_SUCCESSFUL)
		{
			pIRC->SetBlocking(false);
			sq->pushbool(v, SQTrue);
			return 1;
		}
		else
		{
			IRCMessage("Error connecting to IRC. You probably forgot something.");
			sq->pushbool(v, SQFalse);
			return 1;
		}
	}

	sq->pushbool( v, SQFalse );
	return 1;
}

_SQUIRRELDEF( IRC_Join )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 3)
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		if (pIRC && pIRC->IsConnected())
		{
			if (sq->gettype(v, 3) == OT_STRING)
			{
				const SQChar *szChannel;
				const SQChar *szPass = "";

				sq->getstring(v, 3, &szChannel);
				if (iArgCount >= 4 && sq->gettype(v, 4) == OT_STRING)
					sq->getstring(v, 4, &szPass);

				pIRC->Join(szChannel, szPass);
			}
			else
				return sq->throwerror(v, _SC("Error in 'IRC_Join': Invalid argument type"));
		}
	}

	return 0;
}

_SQUIRRELDEF( IRC_Part )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 3)
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		if (pIRC && pIRC->IsConnected())
		{
			if (sq->gettype(v, 3) == OT_STRING)
			{
				const SQChar *szChannel;
				const SQChar *szReason = "";

				sq->getstring(v, 3, &szChannel);
				if (iArgCount >= 4 && sq->gettype(v, 4) == OT_STRING)
					sq->getstring(v, 4, &szReason);

				pIRC->Part(szChannel, szReason);
			}
			else
				return sq->throwerror(v, _SC("Error in 'IRC_Part': Invalid argument type"));
		}
	}

	return 0;
}

_SQUIRRELDEF( IRC_Topic )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 4)
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		if (pIRC && pIRC->IsConnected())
		{
			if (sq->gettype(v, 3) == OT_STRING && sq->gettype(v, 4) == OT_STRING)
			{
				const SQChar *szChannel;
				const SQChar *szTopic = "";

				sq->getstring(v, 3, &szChannel);
				sq->getstring(v, 4, &szTopic);

				pIRC->Topic(szChannel, szTopic);
			}
			else
				return sq->throwerror(v, _SC("Error in 'IRC_Topic': Invalid argument type"));
		}
	}

	return 0;
}

_SQUIRRELDEF( IRC_PrivMsg )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 4)
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		if (pIRC && pIRC->IsConnected())
		{
			const SQChar *szChannel;
			const SQChar *szMessage;

			sq->getstring(v, 3, &szChannel);
			sq->getstring(v, 4, &szMessage);

			pIRC->PrivMsg(szChannel, szMessage);
		}
	}

	return 0;
}

_SQUIRRELDEF( IRC_Action )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 4)
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		if (pIRC && pIRC->IsConnected())
		{
			if (sq->gettype(v, 3) == OT_STRING && sq->gettype(v, 4) == OT_STRING)
			{
				const SQChar *szChannel;
				const SQChar *szText;

				sq->getstring(v, 3, &szChannel);
				sq->getstring(v, 4, &szText);

				pIRC->Action(szChannel, szText);
			}
			else
				return sq->throwerror(v, _SC("Error in 'IRC_Action': Invalid argument type"));
		}
	}

	return 0;
}

_SQUIRRELDEF( IRC_Notice )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 4)
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		if (pIRC && pIRC->IsConnected())
		{
			if (sq->gettype(v, 3) == OT_STRING && sq->gettype(v, 4) == OT_STRING)
			{
				const SQChar *szChannel;
				const SQChar *szText;

				sq->getstring(v, 3, &szChannel);
				sq->getstring(v, 4, &szText);

				pIRC->Notice(szChannel, szText);
			}
			else
				return sq->throwerror(v, _SC("Error in 'IRC_Notice': Invalid argument type"));
		}
	}

	return 0;
}

_SQUIRRELDEF( IRC_Mode )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 4)
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		if (pIRC && pIRC->IsConnected())
		{
			if (sq->gettype(v, 3) == OT_STRING && sq->gettype(v, 4) == OT_STRING)
			{
				const SQChar *szChannel;
				const SQChar *szModes;

				sq->getstring(v, 3, &szChannel);
				sq->getstring(v, 4, &szModes);

				pIRC->Mode(szChannel, szModes);
			}
			else
				return sq->throwerror(v, _SC("Error in 'IRC_Mode': Invalid argument type"));
		}
	}

	return 0;
}

_SQUIRRELDEF( IRC_Quit )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 2)
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		if (pIRC && pIRC->IsConnected())
		{
			const SQChar *szMessage = "SqIRC by Thijn";
			if (iArgCount >= 3 && sq->gettype(v, 3) == OT_STRING)
				sq->getstring(v, 3, &szMessage);

			pIRC->Quit(szMessage);
		}
	}

	return 0;
}

_SQUIRRELDEF( IRC_Invite )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 4)
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		if (pIRC && pIRC->IsConnected())
		{
			if (sq->gettype(v, 3) == OT_STRING && sq->gettype(v, 4) == OT_STRING)
			{
				const SQChar *szChannel;
				const SQChar *szUser;

				sq->getstring(v, 3, &szUser);
				sq->getstring(v, 4, &szChannel);

				pIRC->Invite(szUser, szChannel);
			}
			else
				return sq->throwerror(v, _SC("Error in 'IRC_Invite': Invalid argument type"));
		}
	}

	return 0;
}

_SQUIRRELDEF( IRC_GetUserModes )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 4)
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		if (pIRC && pIRC->IsConnected())
		{
			if (sq->gettype(v, 3) == OT_STRING && sq->gettype(v, 4) == OT_STRING)
			{
				const SQChar *szChannel;
				const SQChar *szUser;

				sq->getstring(v, 3, &szChannel);
				sq->getstring(v, 4, &szUser);

				CIRCChannel *pChannel = pIRC->GetChannels()->Find(szChannel);
				if (pChannel)
				{
					const SQChar *szUserModes = pChannel->GetUserModes(szUser);
					sq->pushstring(v, szUserModes, sizeof(szUserModes));
					return 1;
				}
				else
					return sq->throwerror(v, _SC("Error in 'IRC_GetUserModes': Unknown Channelname"));
			}
			else
				return sq->throwerror(v, _SC("Error in 'IRC_GetUserModes': Invalid argument type"));
		}
	}

	sq->pushnull( v );
	return 1;
}

_SQUIRRELDEF( IRC_GetNetworkName )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 2)
	{
		CIRC* pIRC;
		sq->getuserpointer(v, 2, (SQUserPointer*)&pIRC);

		if (pIRC && pIRC->IsConnected())
		{
			const SQChar *szNetworkName = pIRC->GetServerInfo()->GetNetworkName();
			sq->pushstring(v, szNetworkName, sizeof(szNetworkName));
			return 1;
		}
	}

	sq->pushnull( v );
	return 1;
}

/*
* IRC_Join(irc, channel, <pass>) - Joins the specified channel. Password is optional
* IRC_Part(irc, channel, <reason>) - Leaves the specified channel. Reason is optional
* IRC_Topic(irc, channel, topic) - Sets the channels topic
* IRC_PrivMsg(irc, channel/nick, text) - Sends a message to the channel/user
* IRC_Action(irc, channel/nick, text) - Sends a /me to the channel/user
* IRC_Notice(irc, channel/nick, text) - Sends a notice to the channel/user
* IRC_Mode(irc, channel/nick, text) - Changes a channel/users modes
* IRC_Quit(irc, reason) - Exits the IRC server
* IRC_Invite(irc, user, channel) - Invites a user into the channel
* IRC_GetUserModes(irc, channel, user) - Returns the specified users modes on the channel
* IRC_GetNetworkName(irc) - Returns the network name (may not work on some servers)
*/

void irc_KeepAlive()
{
	IRCLinkedList* node = head;
	while (node != NULL)
	{
		CIRC* pIRC = node->pIRC;
		if (pIRC != NULL && pIRC->IsConnected())
			pIRC->Listen();

		node = node->next;
	}
}

SQInteger RegisterSquirrelFunc( HSQUIRRELVM v, SQFUNCTION f, const SQChar* fname, unsigned char ucParams, const SQChar* szParams )
{
	char szNewParams[ 32 ];

	sq->pushroottable( v );
	sq->pushstring( v, fname, -1 );
	sq->newclosure( v, f, 0 ); /* create a new function */

	if ( ucParams > 0 ) 
	{
		ucParams++; /* This is to compensate for the root table */
		
		sprintf( szNewParams, "t%s", szParams );
		sq->setparamscheck( v, ucParams, szNewParams ); /* Add a param type check */
	}

	sq->setnativeclosurename( v, -1, fname );
	sq->newslot( v, -3, SQFalse );
	sq->pop( v, 1 ); /* pops the root table */

	return 0;
}

void RegisterFuncs( HSQUIRRELVM v )
{
	RegisterSquirrelFunc( v, IRC_Init, "IRC_Init", 0, "" );
	RegisterSquirrelFunc( v, IRC_Connect, "IRC_Connect", 2, "pb" );
	RegisterSquirrelFunc( v, IRC_SetIP, "IRC_SetIP", 2, "ps" );
	RegisterSquirrelFunc( v, IRC_SetPort, "IRC_SetPort", 2, "pi" );
	RegisterSquirrelFunc( v, IRC_SetNick, "IRC_SetNick", 2, "ps" );
	RegisterSquirrelFunc( v, IRC_SetUser, "IRC_SetUser", 2, "ps" );
	RegisterSquirrelFunc( v, IRC_SetRealname, "IRC_SetRealname", 2, "ps" );
	RegisterSquirrelFunc( v, IRC_SetPass, "IRC_SetPass", 2, "ps" );
	RegisterSquirrelFunc( v, IRC_Disconnect, "IRC_Disconnect", 1, "p" );

	RegisterSquirrelFunc( v, IRC_Join, "IRC_Join", 0, 0);
	RegisterSquirrelFunc( v, IRC_Part, "IRC_Part", 0, 0);
	RegisterSquirrelFunc( v, IRC_Topic, "IRC_Topic", 3, "pss");
	RegisterSquirrelFunc( v, IRC_PrivMsg, "IRC_PrivMsg", 3, "pss");
	RegisterSquirrelFunc( v, IRC_Notice, "IRC_Notice", 3, "pss");
	RegisterSquirrelFunc( v, IRC_Mode, "IRC_Mode", 3, "pss");
	RegisterSquirrelFunc( v, IRC_Quit, "IRC_Quit", 0, 0);
	RegisterSquirrelFunc( v, IRC_Invite, "IRC_Invite", 3, "pss");
	RegisterSquirrelFunc( v, IRC_Action, "IRC_Action", 3, "pss");
	RegisterSquirrelFunc( v, IRC_GetUserModes, "IRC_GetUserModes", 3, "pss");
	RegisterSquirrelFunc( v, IRC_GetNetworkName, "IRC_GetNetworkName", 1, "p");
}
