//
// SQFuncs: These are the tools to register custom functions within the Squirrel VM.
//
//	Written for Liberty Unleashed by the Liberty Unleashed Team.
//

#pragma once
#ifndef _SQFUNCS_H
#define _SQFUNCS_H

#include "SQMain.h"
#include "squirrel.h"
#include "vIRC/Source/IRC.h"

#define _SQUIRRELDEF( x ) SQInteger x( HSQUIRRELVM v )

#ifndef WIN32
#define __stdcall
#endif

typedef struct IRCLinkedList IRCLinkedList;

#ifdef __cplusplus
extern "C" {
#endif

	SQInteger				RegisterSquirrelFunc				( HSQUIRRELVM v, SQFUNCTION f, const SQChar* fname, unsigned char uiParams, const SQChar* szParams );
	void					RegisterFuncs						( HSQUIRRELVM v );
	void					RegisterConst						(HSQUIRRELVM v, const char * name, const char* value);
	void					irc_Message							( const char* szWho, const char* szText );
	void					irc_KeepAlive						();

	int __stdcall RawOutput( const char* szHost, const char* szData, CIRC* pIRC );
	int __stdcall PrivMsg( const char* szHost, const char* szData, CIRC* pIRC );
	int __stdcall Connected( const char* szHost, const char* szData, CIRC* pIRC );
	int __stdcall Disconnected( CIRC* pIRC );

#ifdef __cplusplus
}
#endif

#endif
