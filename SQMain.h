//
// SQMain: This is where the module is loaded/unloaded and initialised.
//
//	Written for Liberty Unleashed by the Liberty Unleashed Team.
//

#pragma once
#include <string.h>
#include <stdio.h>
#include "SQImports.h"

// Squirrel
extern "C"
{
	#include "squirrel.h"

	void OnFrame               ( float fElapsedTime );
	void OnPlayerConnect       ( int nPlayerId );
	void OnPlayerDisconnect    ( int nPlayerId, int nReason );
	void OnPlayerDeath         ( int nPlayerId, int nKillerId, int nReason, int nBodyPart );
	int  OnPublicMessage       ( int nPlayerId, const char* pszText );
}