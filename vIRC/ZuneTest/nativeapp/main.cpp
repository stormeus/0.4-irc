﻿/*  _____                         ________   ____    __  __
 * /\  __`\                      /\_____  \ /\  _`\ /\ \/\ \
 * \ \ \/\ \  _____     __    ___\/____//'/'\ \ \/\ \ \ \/'/'
 *  \ \ \ \ \/\ '__`\ /'__`\/' _ `\   //'/'  \ \ \ \ \ \ , <
 *   \ \ \_\ \ \ \L\ \\  __//\ \/\ \ //'/'___ \ \ \_\ \ \ \\`\
 *    \ \_____\ \ ,__/ \____\ \_\ \_\/\_______\\ \____/\ \_\ \_\
 *     \/_____/\ \ \/ \/____/\/_/\/_/\/_______/ \/___/  \/_/\/_/
 *              \ \_\
 *               \/_/ OpenZDK Release 1 | 2010-04-14
 *
 * main.cpp
 * Copyright (c) 2010 itsnotabigtruck.
 * No rights reserved.
 * 
 * All rights are waived to the maximum extent possible; see
 * http://creativecommons.org/publicdomain/zero/1.0/ for more information
 *
 * Replace this banner when writing your own applications
 */

#include <windows.h>
#include <zdk.h>
#include "../../Source/IRC.h"

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd)
{
	ZDK_INPUT_STATE input;
	ZDKSystem_ShowSplashScreen(false);

	CIRCManager::Init();

	CIRC* pIRC = CIRCManager::New();
	if ( pIRC )
	{
		pIRC->SetIP( "178.32.109.77" );
		pIRC->SetPort( 6667 );
		pIRC->SetNick( "BobZune" );
		pIRC->SetJoinOnInvite( true );
		//pIRC->SetRawCallback( RawOutput );
/*		pIRC->AddIRCCmdHook( "PRIVMSG", PrivMsg );
		pIRC->AddNumericIRCCmdHook( RPL_ISUPPORT, ISupport );*/

		printf( "Starting connection\n" );
		if ( pIRC->Connect() == IRC_CONN_SUCCESSFUL )
		{
			printf( "Connected!\n" );
			while ( true )
			{
				unsigned int ui = pIRC->Listen();
				if ( ui )
				{
					//printf( "Host: %s, Data: %s\n", pPacket->szHost, pPacket->szData );
				}

				ZDKInput_GetState(&input);
				if (input.TouchState.Count > 0)
					break;

				Sleep( 5 );
			}

			pIRC->Disconnect();
		}
	}
	return 0;
}
