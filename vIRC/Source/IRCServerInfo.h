#pragma once

#include "IRCCommon.h"

#ifndef _IRC_PREFIX_MODES_SIZE
	#define _IRC_PREFIX_MODES_SIZE 6
#endif

struct _IRC_PREFIX_MODES
{
	_IRC_PREFIX_MODES()
		: cPrefix( 0 ), cMode( 0 )
	{
	}

	char cPrefix;
	char cMode;
};

class CIRCServerInfo
{
public:
	CIRCServerInfo(void);
	~CIRCServerInfo(void);

	void								ParseISupportMode					( const char* sz, const size_t uiTokens = 1 );

	const char*							GetNetworkName						( void ) const						{ return m_szNetworkName; }
	void								SetNetworkName						( const char* sz )					{ irc_strcpy( m_szNetworkName, sz, sizeof( m_szNetworkName ) ); }

	// Nick Stuff
	unsigned int						GetMaxNickLen						( void ) const						{ return m_uiMaxNickLen; }
	void								SetMaxNickLen						( const unsigned int ui )			{ m_uiMaxNickLen = ui; }

	// Channel Stuff
	unsigned int						GetMaxChanNameLen					( void ) const						{ return m_uiMaxChanNameLen; }
	void								SetMaxChanNameLen					( const unsigned int ui )			{ m_uiMaxChanNameLen = ui; }

	unsigned int						GetMaxChanTopicLen					( void ) const						{ return m_uiMaxChanTopicLen; }
	void								SetMaxChanTopicLen					( const unsigned int ui )			{ m_uiMaxChanTopicLen = ui; }

	unsigned int						GetMaxChanModes						( void ) const						{ return m_uiMaxChanModes; }
	void								SetMaxChanModes						( const unsigned int ui )			{ m_uiMaxChanModes = ui; }

	unsigned int						GetMaxChannels						( void ) const						{ return m_uiMaxChannels; }
	void								SetMaxChannels						( const unsigned int ui )			{ m_uiMaxChannels = ui; }

	// Misc Stuff
	unsigned int						GetMaxKickReasonLen					( void ) const						{ return m_uiMaxKickReasonLen; }
	void								SetMaxKickReasonLen					( const unsigned int ui )			{ m_uiMaxKickReasonLen = ui; }

	unsigned int						GetMaxAwayReasonLen					( void ) const						{ return m_uiMaxAwayReasonLen; }
	void								SetMaxAwayReasonLen					( const unsigned int ui )			{ m_uiMaxAwayReasonLen = ui; }

	_IRC_PREFIX_MODES					GetPrefixMode						( const size_t ui )					{ return m_sPrefixModes[ ui ]; }

private:
	char*								m_szNetworkName;
	// Nick Stuff
	unsigned int						m_uiMaxNickLen;
	// Channel Stuff
	unsigned int						m_uiMaxChanNameLen;
	unsigned int						m_uiMaxChanTopicLen;
	unsigned int						m_uiMaxChanModes;
	unsigned int						m_uiMaxChannels;
	// Misc Stuff
	unsigned int						m_uiMaxKickReasonLen;
	unsigned int						m_uiMaxAwayReasonLen;

	bool								m_bExcepts;
	bool								m_bInvex;

	_IRC_PREFIX_MODES					m_sPrefixModes[ _IRC_PREFIX_MODES_SIZE ];
};
