#include "IRC.h"
#include <assert.h>

CIRC::CIRC( unsigned char uc )
: m_ucID( uc ), m_usPort( 6667 ), m_bConnected( false ), m_bJoinOnInvite( false ), m_bUsingSSL( false ), m_pSocket( INVALID_SOCKET ),
m_pCmdHooks( 0 ), m_pRawCallback( 0 ), m_pDisconnectCallback( 0 )
{
	*m_szIP = 0;

	*m_szPass = 0;
	*m_szNick = 0;
	strcpy( m_szUsername, "VIRC-Lib" );
	strcpy( m_szRealname, "Unknown" );

	m_szVersionReply = 0;

	m_pServerInfo = new(std::nothrow) CIRCServerInfo();
	m_pUsers = new(std::nothrow) CIRCUsers( m_pServerInfo );
	m_pChannels = new(std::nothrow) CIRCChannels( m_pServerInfo, m_pUsers );

	for( unsigned int ui = 0; ui < _IRC_SERVER_REPLIES_SIZE; ++ui )
		m_pNumericCmdHooks[ ui ] = 0;

#if _IRC_OPENSSL_SUPPORT==1
	m_pSSLContext = 0;
	m_pSSLSocket = 0;
#endif
}

CIRC::~CIRC(void)
{
	Disconnect();

	DelAllIRCCmdHooks();
	DelAllNumericIRCCmdHooks();

	m_pDisconnectCallback = 0;
	m_pRawCallback = 0;

	if ( m_szVersionReply )
	{
		delete [] m_szVersionReply;
		m_szVersionReply = 0;
	}

	if ( m_pChannels )
	{
		delete m_pChannels;
		m_pChannels = 0;
	}

	if ( m_pUsers )
	{
		delete m_pUsers;
		m_pUsers = 0;
	}

	if ( m_pServerInfo )
	{
		delete m_pServerInfo;
		m_pServerInfo = 0;
	}
}

void CIRC::SetNick( const char* sz )
{
	if ( m_bConnected ) 
		IRCSend( "NICK %s", sz );
	// If we are conneted, wait for the server to confirm our name change
	else
		irc_strcpy( m_szNick, sz, sizeof( m_szNick ) );
}

void CIRC::SetVersionReply( const char* sz )
{
	size_t uiLen = strlen( sz );

	if ( m_szVersionReply )
	{
		delete [] m_szVersionReply;
		m_szVersionReply = 0;
	}

	m_szVersionReply = new char [ uiLen + 1 ];
	irc_strcpy( m_szVersionReply, sz, uiLen+1 );
}

void CIRC::SetBlocking( bool b )
{
	unsigned long iBlock = b ? 0 : 1;

#if defined( WIN32 ) || defined( _WIN64 )
	if ( m_pSocket ) 
		ioctlsocket( m_pSocket, FIONBIO, &iBlock );
#else
	if ( m_pSocket ) 
		ioctl( m_pSocket, FIONBIO, &iBlock );
#endif
}

// Do funky stuff below
_IRC_CONNECTION_RETURNS CIRC::Connect( void )
{
	if ( !*m_szIP ) return IRC_CONN_NOIP;
	if ( !m_usPort ) return IRC_CONN_NOPORT;
	if ( !*m_szNick ) return IRC_CONN_NONICK;
	if ( !*m_szUsername ) return IRC_CONN_NOUSER;
	if ( !*m_szRealname ) return IRC_CONN_NOREALNAME;

	if ( m_pSocket != INVALID_SOCKET ) return IRC_CONN_ALREADYCONNECTED;

	struct addrinfo *result = NULL;
    struct addrinfo *ptr = NULL;
    struct addrinfo hints;

	memset( &hints, 0, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

	char szPort[ 8 ] = { 0 };
	sprintf( szPort, "%u", m_usPort );
	if ( getaddrinfo( m_szIP, szPort, &hints, &result ) != 0 )
	{
		// FAIL!
		return IRC_CONN_FAILED;
	}

	for ( ptr = result; ptr != NULL; ptr = ptr->ai_next )
	{
		m_pSocket = socket( result->ai_family, result->ai_socktype, result->ai_protocol );

		if ( m_pSocket != INVALID_SOCKET )
		{
#if _IRC_OPENSSL_SUPPORT==1
			if ( m_bUsingSSL )
			{
				ERR_load_crypto_strings();
				SSL_load_error_strings();
				m_pSSLContext = SSL_CTX_new( SSLv23_client_method() );
				if ( !m_pSSLContext )
				{
					// SSL Failed
					char szOut[ 256 ] = { 0 };
					ERR_error_string( ERR_get_error(), szOut );
					printf( szOut );
					m_bUsingSSL = false;
				}
				else
				{
					m_pSSLSocket = SSL_new( m_pSSLContext );
					if ( !m_pSSLSocket )
					{
						// SSL Failed
						SSL_CTX_free( m_pSSLContext );
						m_pSSLContext = 0;

						m_bUsingSSL = false;
					}
				}
			}
#endif
			if ( connect( m_pSocket, result->ai_addr, (int)result->ai_addrlen ) != SOCKET_ERROR )
			{
#if _IRC_OPENSSL_SUPPORT==1
				if ( ( m_bUsingSSL ) && ( m_pSSLSocket ) )
				{
					SSL_set_fd( m_pSSLSocket, (int)m_pSocket );
					if (SSL_connect( m_pSSLSocket ) != 1)
					{
						// SSL Failed
						Disconnect();

						freeaddrinfo( result );

						return IRC_CONN_FAILED;
					}
				}
#endif

				m_bConnected = true;

				IRCSend( "USER %s 0 * :%s", m_szUsername, m_szRealname );
				IRCSend( "NICK %s", m_szNick );
				if ( *m_szPass ) IRCSend( "PASS %s", m_szPass );

				freeaddrinfo( result );

				return IRC_CONN_SUCCESSFUL;
			}
		}
	}

	freeaddrinfo( result );

	return IRC_CONN_FAILED;
}

void CIRC::Disconnect( void )
{
	Quit( "Client Exited" );

	m_bConnected = false;

#if _IRC_OPENSSL_SUPPORT==1
	if ( ( m_bUsingSSL ) && ( m_pSSLSocket ) )
	{
		SSL_shutdown( m_pSSLSocket );
		SSL_free( m_pSSLSocket );

		if ( m_pSSLContext )
		{
			SSL_CTX_free( m_pSSLContext );
			m_pSSLContext = 0;
		}
		m_pSSLSocket = 0;
	}
#endif

	if ( m_pSocket )
	{
#if defined( WIN32 ) || defined( _WIN64 )
		shutdown( m_pSocket, 2 );
		closesocket( m_pSocket );
#else		
		shutdown( m_pSocket, SHUT_RDWR );
		close( m_pSocket );
#endif
		m_pSocket = INVALID_SOCKET;
	}

	// If we are disconnecting, we should free up the memory we used
	// Keep serverinfo and the class instances intact though, incase we reconnect
	m_pChannels->RemoveAll();
	m_pUsers->RemoveAll();
}

unsigned int CIRC::Listen( void )
{
	if ( ( m_bConnected ) && ( m_pSocket ) )
	{
		// The IRC protocol has a max message size of 512 bytes, including the \r\n
		// Scrap that, windows seems to buffer everything
		char szBuffer[ 4096 ] = { 0 };
		int iLen = SOCKET_ERROR;

#if _IRC_OPENSSL_SUPPORT==1
		if ( m_bUsingSSL )
		{
			if ( m_pSSLSocket )
				iLen = SSL_read( m_pSSLSocket, szBuffer, sizeof( szBuffer ) );
		}
		else
#endif
			iLen = recv( m_pSocket, szBuffer, sizeof( szBuffer ), 0 );

		if ( iLen == SOCKET_ERROR )
			return 1;

		if ( !iLen )
		{
			Disconnect();
			// Nothing was received, which means we were disconnected
			if ( m_pDisconnectCallback )
			{
				IRCDisconnectCallback pFunc = (IRCDisconnectCallback)m_pDisconnectCallback;
				pFunc( this );
			}
			return 1;
		}
		
		ParseIRCReplies( szBuffer );
	}

	return 0;
}

void CIRC::ParseIRCReplies( char* szBuffer )
{
	char* szContext;
	// Changed to strtok due to a bug
	char* p = irc_strtok_s( szBuffer, "\r\n", &szContext );

	while ( p )
	{		
		if ( *p )
		{
			size_t iLen = strlen( p );
			static char szTmp[ 512 ] = { 0 };
			irc_strcpy( szTmp, p, sizeof( szTmp ) );

			static char szHost[ 128 ] = { 0 };
			static char szData[ 512 ] = { 0 };
			irc_strcpy( szHost, strtok( szTmp, " " ), sizeof( szHost ) );
			irc_strcpy( szData, p + strlen( szHost ) + 1, sizeof( szData ) );

			irc_strcpy( szTmp, szData, sizeof( szTmp ) );

			static char szCmd[ 64 ] = { 0 };
			size_t uiTokens = NumTok( szTmp, ' ' );

			if ( uiTokens ) 
			{
				irc_strcpy( szCmd, strtok( szTmp, " " ), 64 );
				irc_strcpy( szData, szData + strlen( szCmd ) + 1, 512 );
			}

			if ( irc_isequal( szHost, "PING" ) ) IRCSend( "PONG %s", szCmd );
			else if ( irc_isequal( szHost, "ERROR" ) )
			{
				// An error occured
			}
			else if ( szHost[ 0 ] == ':' )
			{
#pragma region Numerics
				if ( IsNum( szCmd ) )
				{
					unsigned int uiNumeric = atoi( szCmd );
					switch( uiNumeric )
					{
					case RPL_ISUPPORT:
						{
							if ( uiTokens >= 3 )
							{
								irc_strcpy( szTmp, szData, sizeof( szTmp ) );
								char* szNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
								irc_strcpy( szNick, strtok( szTmp, " " ), m_pServerInfo->GetMaxNickLen() );
								if ( !strcmp( szNick, m_szNick ) )
								{
									irc_strcpy( szTmp, szData + strlen( szNick ) + 1, sizeof( szTmp ) );
									char* szThing = strstr( szTmp, ":are supported" );
									*szThing = '\0';
									irc_strcpy( szTmp, szTmp, szThing - szTmp );
									uiTokens = NumTok( szTmp, ' ' );

									static char szSupports[ 64 ][ 160 ] = { 0 };
									irc_strcpy( szTmp, strtok( szTmp, " " ), sizeof( szTmp ) );
									for ( size_t ui = 0; ui < uiTokens-1; ++ui )
									{
										irc_strcpy( szSupports[ ui ], szTmp, 160 );
										irc_strcpy( szTmp, strtok( 0, " " ), sizeof( szTmp ) );
									}
									irc_strcpy( szSupports[ uiTokens-1 ], szTmp, 160 );

									for ( size_t ui = 0; ui < uiTokens; ++ui )
									{
										m_pServerInfo->ParseISupportMode( szSupports[ ui ], NumTok( szSupports[ ui ], '=' ) );
									}
								}
							}
							break;
						}

					case RPL_CHANNELMODEIS:
						{
							// Nick Channel <Modes>
							if ( uiTokens >= 4 )
							{
								strcpy( szTmp, szData );
								char* szNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
								irc_strcpy( szNick, strtok( szTmp, " " ), m_pServerInfo->GetMaxNickLen() );
								if ( irc_isequal( szNick, m_szNick ) )
								{
									char* szChannel = (char*)alloca( m_pServerInfo->GetMaxChanNameLen() );
									irc_strcpy( szChannel, strtok( 0, " " ), m_pServerInfo->GetMaxChanNameLen() );
									CIRCChannel* pChan = m_pChannels->Find( szChannel );
									if ( pChan )
									{
										char* szModes = (char*)alloca( 32 );
										irc_strcpy( szModes, szData + strlen( szNick ) + 1 + strlen( szChannel ) + 1, 32 );

										pChan->SetChannelModes( szModes );
									}
								}
							}			
							break;
						}

					case RPL_TOPIC:
						{
							// Nick Channel :Topic
							if ( uiTokens >= 4 )
							{
								strcpy( szTmp, szData );
								char* szNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
								irc_strcpy( szNick, strtok( szTmp, " " ), m_pServerInfo->GetMaxNickLen() );
								if ( irc_isequal( szNick, m_szNick ) )
								{
									char* szChannel = (char*)alloca( m_pServerInfo->GetMaxChanNameLen() );
									irc_strcpy( szChannel, strtok( 0, " " ), m_pServerInfo->GetMaxChanNameLen() );
									CIRCChannel* pChan = m_pChannels->Find( szChannel );
									if ( pChan )
									{
										char* szTopic = (char*)alloca( m_pServerInfo->GetMaxChanTopicLen() );
										irc_strcpy( szTopic, szData + strlen( szNick ) + 1 + strlen( szChannel ) + 1 + 1, m_pServerInfo->GetMaxChanTopicLen() );

										pChan->SetTopic( szTopic );
									}
								}
							}
							break;
						}

					case RPL_TOPICINFO:
						{
							// Nick Channel Setter Time
							if ( uiTokens == 5 )
							{
								strcpy( szTmp, szData );
								char* szNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
								irc_strcpy( szNick, strtok( szTmp, " " ), m_pServerInfo->GetMaxNickLen() );
								if ( irc_isequal( szNick, m_szNick ) )
								{
									char* szChannel = (char*)alloca( m_pServerInfo->GetMaxChanNameLen() );
									irc_strcpy( szChannel, strtok( 0, " " ), m_pServerInfo->GetMaxChanNameLen() );
									CIRCChannel* pChan = m_pChannels->Find( szChannel );
									if ( pChan )
									{
										char* szSetter = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
										irc_strcpy( szSetter, strtok( 0, " " ), m_pServerInfo->GetMaxNickLen() );

										pChan->SetTopicSetter( szSetter );

										char szSetTime[ 32 ] = { 0 };
										irc_strcpy( szSetTime, strtok( 0, " " ), sizeof( szSetTime ) );
										unsigned int uiSetTime = 0;
										if ( IsNum( szSetTime ) ) uiSetTime = (unsigned int)atoi( szSetTime );

										pChan->SetTopicSetTime( uiSetTime );
									}
								}
							}
							break;
						}

					case RPL_NAMREPLY:
						{
							// Nick =/@ Channel :<UserList>
							if ( uiTokens >= 5 )
							{
								strcpy( szTmp, szData );
								char* szNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
								irc_strcpy( szNick, strtok( szTmp, " " ), m_pServerInfo->GetMaxNickLen() );
								if ( irc_isequal( szNick, m_szNick ) )
								{
									// Dont know wtf this is...
									strtok( 0, " " );

									char* szChannel = (char*)alloca( m_pServerInfo->GetMaxChanNameLen() );
									irc_strcpy( szChannel, strtok( 0, " " ), m_pServerInfo->GetMaxChanNameLen() );
									CIRCChannel* pChan = m_pChannels->Find( szChannel );
									if ( pChan )
									{
										/*irc_strcpy( szTmp, szData + strlen( szNick ) + 3 + strlen( szChannel ) + 2, sizeof( szTmp ) );
										uiTokens = NumTok( szTmp, ' ' );*/

										irc_strcpy( szTmp, strtok( 0, " " ), sizeof( szTmp ) );
										irc_strcpy( szTmp, szTmp+1, sizeof( szTmp ) );
										pChan->AddUser( szTmp );
										for ( size_t ui = 0; ui < uiTokens-5; ++ui )
										{
											irc_strcpy( szTmp, strtok( 0, " " ), sizeof( szTmp ) );
											pChan->AddUser( szTmp );
										}										
									}
								}
							}
							break;
						}
					}

					CallNumericIRCCmdHook( uiNumeric, szHost, szData );
				}
#pragma endregion
				else
				{
					// This statement will parse any internal stuff
					switch ( szCmd[ 0 ] )
					{
					case 'I':
						{
							if ( irc_isequal( szCmd, "INVITE" ) )
							{
								if ( uiTokens >= 3 )
								{
									strcpy( szTmp, szData );
									if ( irc_isequal( strtok( szTmp, " " ), m_szNick ) )
									{
										char* szChannel = (char*)alloca( m_pServerInfo->GetMaxChanNameLen() );
										irc_strcpy( szChannel, strtok( 0, " " ) + 1, m_pServerInfo->GetMaxChanNameLen() );

										if ( m_bJoinOnInvite ) IRCSend( "JOIN %s", szChannel );
									}
								}
							}
							break;
						}

					case 'J':
						{
							if ( irc_isequal( szCmd, "JOIN" ) )
							{
								if ( uiTokens >= 2 )
								{
									strcpy( szTmp, szHost );
									char* szNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
									irc_strcpy( szNick, strtok( szTmp, "!" ) + 1, m_pServerInfo->GetMaxNickLen() );
									strcpy( szTmp, szData );
									char* szChannel = (char*)alloca( m_pServerInfo->GetMaxChanNameLen() );
									irc_strcpy( szChannel, szTmp + 1, m_pServerInfo->GetMaxChanNameLen() );

									if ( irc_isequal( szNick, m_szNick ) )
									{
										// We have joined a channel
										m_pChannels->New( szChannel );
										// Get the channel modes on join
										IRCSend( "MODE %s", szChannel );
									}
									else
									{
										// Somebody has joined
										CIRCChannel* pChan = m_pChannels->Find( szChannel );
										if ( pChan )
										{
											pChan->AddUser( szNick );
										}
									}
								}
							}
							break;
						}

					case 'K':
						{
							if ( irc_isequal( szCmd, "KICK" ) )
							{
								if ( uiTokens >= 4 )
								{
									strcpy( szTmp, szData );
									char* szChannel = (char*)alloca( m_pServerInfo->GetMaxChanNameLen() );
									irc_strcpy( szChannel, strtok( szTmp, " " ), m_pServerInfo->GetMaxChanNameLen() );

									char* szNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
									irc_strcpy( szNick, strtok( 0, " " ), m_pServerInfo->GetMaxNickLen() );

									char* szReason = (char*)alloca( m_pServerInfo->GetMaxKickReasonLen() );
									irc_strcpy( szReason, strtok( 0, " " ) + 1, m_pServerInfo->GetMaxKickReasonLen() );
									if ( irc_isequal( szNick, m_szNick ) )
									{
										// We were kicked from a channel
										m_pChannels->Remove( szChannel );
									}
									else
									{
										// Somebody has been kicked from a channel
										CIRCChannel* pChan = m_pChannels->Find( szChannel );
										if ( pChan )
										{
											pChan->RemoveUser( szNick );
										}
									}
								}
							}
							else if ( irc_isequal( szCmd, "KILL" ) )
							{
								if ( uiTokens >= 4 )
								{
									strcpy( szTmp, szHost );
									char* szKiller = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
									irc_strcpy( szKiller, strtok( szTmp, "!" ) + 1, m_pServerInfo->GetMaxNickLen() );

									strcpy( szTmp, szData );
									char* szNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
									irc_strcpy( szNick, strtok( szTmp, " " ), m_pServerInfo->GetMaxNickLen() );

									char* szReason = (char*)alloca( m_pServerInfo->GetMaxKickReasonLen() );
									irc_strcpy( szReason, strtok( 0, " " ) + 1, m_pServerInfo->GetMaxKickReasonLen() );
									if ( irc_isequal( szNick, m_szNick ) )
									{
										// We were killed
										// Disconnect before the callback else a reconnect wont work
										Disconnect();

										if ( m_pDisconnectCallback )
										{
											IRCDisconnectCallback pFunc = (IRCDisconnectCallback)m_pDisconnectCallback;
											pFunc( this );
										}
									}
								}
							}
							break;
						}

					case 'M':
						{
							if ( irc_isequal( szCmd, "MODE" ) )
							{
								if ( uiTokens >= 3 )
								{
									strcpy( szTmp, szData );

									char* szChannel = (char*)alloca( m_pServerInfo->GetMaxChanNameLen() );
									irc_strcpy( szChannel, strtok( szTmp, " " ), m_pServerInfo->GetMaxChanNameLen() );
									if ( irc_isequal( szChannel, m_szNick ) )
									{
										// Mode was set on us
									}
									else
									{
										CIRCChannel* pChan = m_pChannels->Find( szChannel );
										if ( pChan )
										{
											char* szModes = (char*)alloca( 32 );
											irc_strcpy( szModes, szData + strlen( szChannel ) + 1, 32 );

											pChan->SetChannelModes( szModes );
										}
									}
								}
							}
							break;
						}

					case 'N':
						{
							if ( irc_isequal( szCmd, "NICK" ) )
							{
								if ( uiTokens >= 2 )
								{
									strcpy( szTmp, szHost );
									char* szNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
									irc_strcpy( szNick, strtok( szTmp, "!" ) + 1, m_pServerInfo->GetMaxNickLen() );
									strcpy( szTmp, szData );
									char* szNewNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
									irc_strcpy( szNewNick, szTmp+1, m_pServerInfo->GetMaxNickLen() );

									if ( irc_isequal( szNick, m_szNick ) )
									{
										// We changed our nickname
										irc_strcpy( m_szNick, szNewNick, m_pServerInfo->GetMaxNickLen() );
									}
									else
									{
										// Somebody has changed their nick
										CIRCUser* pUser = m_pUsers->Find( szNick );
										if ( pUser )
										{
											// Should probably signal to the channels that they have changed nick
											pUser->SetName( szNewNick );
										}
									}

								}
							}
							break;
						}

					case 'P':
						{
							if ( irc_isequal( szCmd, "PART" ) )
							{
								if ( uiTokens >= 2 )
								{
									strcpy( szTmp, szHost );
									char* szNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
									irc_strcpy( szNick, strtok( szTmp, "!" ) + 1, m_pServerInfo->GetMaxNickLen() );
									strcpy( szTmp, szData );
									char* szChannel = (char*)alloca( m_pServerInfo->GetMaxChanNameLen() );
									irc_strcpy( szChannel, szTmp, m_pServerInfo->GetMaxChanNameLen() );

									if ( irc_isequal( szNick, m_szNick ) )
									{
										// We left a channel
										m_pChannels->Remove( szChannel );
									}
									else
									{
										// Somebody has left a channel
										CIRCChannel* pChan = m_pChannels->Find( szChannel );
										if ( pChan )
										{
											pChan->RemoveUser( szNick );
										}
									}

								}
							}
							else if ( irc_isequal( szCmd, "PRIVMSG" ) )
							{
								if ( uiTokens >= 2 )
								{
									strcpy( szTmp, szHost );
									char* szNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
									irc_strcpy( szNick, strtok( szTmp, "!" ) + 1, m_pServerInfo->GetMaxNickLen() );
									strcpy( szTmp, szData );

									char* szReceiver = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
									irc_strcpy( szReceiver, strtok( szTmp, " " ), m_pServerInfo->GetMaxNickLen() );
									strcpy( szTmp, szData );
									irc_strcpy( szTmp, szTmp + strlen( szReceiver ) + 2, sizeof( szTmp ) );

									// Handle finger, version etc
									if ( ( *szTmp == '\001' ) && ( irc_isequal( szReceiver, m_szNick ) ) )
									{
										if ( irc_isequal( szTmp, "\001VERSION\001" ) )
										{
											if ( m_szVersionReply )
												IRCSend( "NOTICE %s :\001VERSION %s\001", szNick, m_szVersionReply );
											else
												IRCSend( "NOTICE %s :\001VERSION VRockers IRC Library - Version 1.0\001", szNick );
										}
										else if ( irc_isequal( szTmp, "\001FINGER\001" ) )
										{
											IRCSend( "NOTICE %s :\001FINGER %s (%s)\001", szNick, m_szNick, m_szRealname );
										}
									}
								}
							}

							break;
						}

					case 'Q':
						{
							if ( irc_isequal( szCmd, "QUIT" ) )
							{
								if ( uiTokens >= 2 )
								{
									strcpy( szTmp, szHost );
									char* szNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
									irc_strcpy( szNick, strtok( szTmp, "!" ) + 1, m_pServerInfo->GetMaxNickLen() );
									strcpy( szTmp, szData );
									
									// Somebody has quit
									stdext::hash_map< std::string, CIRCChannel* > pList = m_pChannels->GetList();
								
									stdext::hash_map< std::string, CIRCChannel* >::const_iterator iter = pList.begin();
									stdext::hash_map< std::string, CIRCChannel* >::const_iterator iter_end = pList.end();

									CIRCChannel* pChan = 0;
									for( ; iter != iter_end; ++iter )
									{
										pChan = (*iter).second;
										if ( pChan )
											pChan->RemoveUser( szNick );
									}

								}
							}
							break;
						}

					case 'T':
						{
							if ( irc_isequal( szCmd, "TOPIC" ) )
							{
								if ( uiTokens >= 3 )
								{
									strcpy( szTmp, szHost );
									char* szNick = (char*)alloca( m_pServerInfo->GetMaxNickLen() );
									irc_strcpy( szNick, strtok( szTmp, "!" ) + 1, m_pServerInfo->GetMaxNickLen() );
									strcpy( szTmp, szData );
									
									char* szChannel = (char*)alloca( m_pServerInfo->GetMaxChanNameLen() );
									irc_strcpy( szChannel, strtok( szTmp, " " ), m_pServerInfo->GetMaxChanNameLen() );

									CIRCChannel* pChan = m_pChannels->Find( szChannel );
									if ( pChan )
									{
										char* szTopic = (char*)alloca( m_pServerInfo->GetMaxChanTopicLen() );
										irc_strcpy( szTopic, szData + strlen( szChannel ) + 1 + 1, m_pServerInfo->GetMaxChanTopicLen() );

										pChan->SetTopic( szTopic );
										pChan->SetTopicSetter( szNick );
#if !defined (_WIN32_WCE)
										// WinCE doesnt have unix functions such as time() to 'save space in the kernel'
										pChan->SetTopicSetTime( time( 0 ) );
#endif
									}
								}
							}
							break;
						}
					}

					CallIRCCmdHook( szCmd, szHost, szData );
				}

				if ( m_pRawCallback )
				{
					IRCCmdCallback pFunc = (IRCCmdCallback)m_pRawCallback;
					irc_strcpy( szData, p + strlen( szHost ) + 1, sizeof( szData ) );
					pFunc( szHost, szData, this );
				}
			}
		}

		p = irc_strtok_s( 0, "\r\n", &szContext );
	}
}

void CIRC::AddIRCCmdHook( const char* szCmd, void* pCallback )
{
	size_t uiCmdLen = strlen( szCmd ) + 1;

	if ( !m_pCmdHooks )
	{
		m_pCmdHooks = new IRCCommandHook;
		m_pCmdHooks->pPrev = 0;
		m_pCmdHooks->szIRCCmd = new char [uiCmdLen];
		irc_strcpy( m_pCmdHooks->szIRCCmd, szCmd, uiCmdLen );
		m_pCmdHooks->pCallback = pCallback;
		m_pCmdHooks->pNext = 0;
	}
	else
	{
		IRCCommandHook* pHook = m_pCmdHooks;

		while ( pHook )
		{
			if ( !pHook->pNext )
			{
				IRCCommandHook* pNextHook = new IRCCommandHook;
				
				pNextHook->pPrev = pHook;
				pNextHook->szIRCCmd = new char [uiCmdLen];
				irc_strcpy( pNextHook->szIRCCmd, szCmd, uiCmdLen );
				pNextHook->pCallback = pCallback;
				pNextHook->pNext = 0;

				pHook->pNext = pNextHook;

				return;
			}
			pHook = pHook->pNext;
		}
	}
}

void CIRC::DelIRCCmdHook( IRCCommandHook* pCmdHook )
{
	if ( pCmdHook->pPrev ) 
		pCmdHook->pPrev->pNext = pCmdHook->pNext;
	
	if ( pCmdHook->pNext )
		pCmdHook->pNext->pPrev = pCmdHook->pPrev;

	pCmdHook->pPrev = 0;
	pCmdHook->pNext = 0;

	if ( pCmdHook->szIRCCmd )
	{
		delete [] pCmdHook->szIRCCmd;
		pCmdHook->szIRCCmd = 0;
	}

	pCmdHook->pCallback = 0;

	delete pCmdHook;
	pCmdHook = 0;
}

void CIRC::DelAllIRCCmdHooks( void )
{
	if ( m_pCmdHooks )
	{
		IRCCommandHook* pHook = m_pCmdHooks;
		IRCCommandHook* pNextHook = 0;

		while ( pHook )
		{
			pNextHook = pHook->pNext;

			DelIRCCmdHook( pHook );

			pHook = pNextHook;
		}

		m_pCmdHooks = 0;
	}
}

void CIRC::CallIRCCmdHook( const char* szCmd, const char* szHost, const char* szData )
{
	IRCCommandHook* pHook;

	if ( !m_pCmdHooks ) return;

	pHook = m_pCmdHooks;
	while ( pHook )
	{
		if ( ( pHook->szIRCCmd ) && ( irc_isequal( pHook->szIRCCmd, szCmd) ) )
		{
			IRCCmdCallback pFunc = (IRCCmdCallback)pHook->pCallback;
			pFunc( szHost, szData, this);
			break;
		}
		else
		{
			pHook = pHook->pNext;
		}
	}
}

void CIRC::AddNumericIRCCmdHook( const unsigned int uiNumeric, void* pCallback )
{
	if ( !m_pNumericCmdHooks[ uiNumeric ] )
	{
		m_pNumericCmdHooks[ uiNumeric ] = new IRCNumericCommandHook;
		m_pNumericCmdHooks[ uiNumeric ]->pPrev = 0;
		m_pNumericCmdHooks[ uiNumeric ]->pCallback = pCallback;
		m_pNumericCmdHooks[ uiNumeric ]->pNext = 0;
	}
	else
	{
		IRCNumericCommandHook* pHook = m_pNumericCmdHooks[ uiNumeric ];

		while ( pHook )
		{
			if ( !pHook->pNext )
			{
				pHook->pNext = new IRCNumericCommandHook;

				pHook->pNext->pPrev = pHook;

				pHook = pHook->pNext;
				pHook->pCallback = pCallback;
				pHook->pNext = 0;

				return;
			}
			pHook = pHook->pNext;
		}
	}
}

void CIRC::DelNumericIRCCmdHook( IRCNumericCommandHook* pCmdHook )
{
	if ( pCmdHook->pPrev ) 
		pCmdHook->pPrev->pNext = pCmdHook->pNext;
	if ( pCmdHook->pNext )
		pCmdHook->pNext->pPrev = pCmdHook->pPrev;

	pCmdHook->pPrev = 0;
	pCmdHook->pNext = 0;
	pCmdHook->pCallback = 0;

	delete pCmdHook;
	pCmdHook = 0;
}

void CIRC::DelAllNumericIRCCmdHooks( void )
{
	for ( unsigned int ui = 0; ui < _IRC_SERVER_REPLIES_SIZE; ++ui )
	{
		if ( m_pNumericCmdHooks[ ui ] )
		{
			IRCNumericCommandHook* pHook = m_pNumericCmdHooks[ ui ];
			IRCNumericCommandHook* pNextHook = 0;

			while ( pHook )
			{
				pNextHook = pHook->pNext;

				DelNumericIRCCmdHook( pHook );

				pHook = pNextHook;
			}

			m_pNumericCmdHooks[ ui ] = 0;
		}
	}
}

void CIRC::CallNumericIRCCmdHook( const unsigned int uiNumeric, const char* szHost, const char* szData )
{
	IRCNumericCommandHook* pHook;

	if ( !m_pNumericCmdHooks ) 
		return;

	if ( uiNumeric >= _IRC_SERVER_REPLIES_SIZE )
		return;

	// Process the hook for all numerics
	pHook = m_pNumericCmdHooks[ RPL_ALL ];
	while ( pHook )
	{
		IRCNumAllCmdCallback pFunc = (IRCNumAllCmdCallback)pHook->pCallback;
		pFunc( uiNumeric, szHost, szData, this );

		pHook = pHook->pNext;
	}

	pHook = m_pNumericCmdHooks[ uiNumeric ];
	while ( pHook )
	{
		IRCCmdCallback pFunc = (IRCCmdCallback)pHook->pCallback;
		pFunc( szHost, szData, this);

		pHook = pHook->pNext;
	}

}

void CIRC::Join( const char* szChannel, const char* szPass )
{
	if ( !*szPass ) IRCSend( "JOIN %s", szChannel );
	else IRCSend( "JOIN %s %s", szChannel, szPass );
}

void CIRC::Part( const char* szChannel, const char* szReason )
{
	if ( !*szReason ) IRCSend( "PART %s", szChannel );
	else IRCSend( "PART %s %s", szChannel, szReason );
}

void CIRC::Topic( const char* szChannel )
{
	IRCSend( "TOPIC %s", szChannel );
}

void CIRC::Topic( const char* szChannel, const char* szText )
{
	if ( !*szText ) IRCSend( "TOPIC %s :", szChannel );
	else IRCSend( "TOPIC %s :%s", szChannel, szText );
}

void CIRC::PrivMsg( const char* szWho, const char* szText )
{
	IRCSend( "PRIVMSG %s :%s", szWho, szText );
}

void CIRC::Action( const char* szWho, const char* szText )
{
	IRCSend( "PRIVMSG %s :\001ACTION %s\001", szWho, szText );
}

void CIRC::Notice( const char* szWho, const char* szText )
{
	IRCSend( "NOTICE %s :%s", szWho, szText );
}

void CIRC::Oper( const char* szUser, const char* szPass )
{
	IRCSend( "OPER %s %s", szUser, szPass );
}

void CIRC::Mode( const char* szUser, const char* szMode )
{
	IRCSend( "MODE %s %s", szUser, szMode );
}

void CIRC::Quit( const char* szReason )
{
	IRCSend( "QUIT :%s", szReason );
}

void CIRC::Names( const char* szChannel )
{
	if ( *szChannel ) IRCSend( "NAMES %s", szChannel );
	else IRCSend( "NAMES" );
}

void CIRC::List( const char* szChannel )
{
	if ( *szChannel ) IRCSend( "LIST %s", szChannel );
	else IRCSend( "LIST" );
}

void CIRC::Invite( const char* szUser, const char* szChannel )
{
	IRCSend( "INVITE %s %s", szUser, szChannel );
}

void CIRC::Kick( const char* szChannel, const char* szUser, const char* szReason )
{
	if ( *szReason ) IRCSend( "KICK %s %s :%s", szChannel, szUser, szReason );
	else IRCSend( "KICK %s %s", szChannel, szUser );
}

// Server Queries and Commands
void CIRC::Version( const char* szServer )
{
	if ( *szServer ) IRCSend( "VERSION %s", szServer );
	else IRCSend( "VERSION" );
}

void CIRC::Stats( const char* szQuery, const char* szServer )
{
	if ( *szServer ) IRCSend( "STATS %s %s", szQuery, szServer );
	else IRCSend( "STATS %s", szQuery );
}

void CIRC::Links( const char* szServerMask, const char* szRemoteServer )
{
	if ( *szServerMask )
	{
		if ( *szRemoteServer ) IRCSend( "LINKS %s %s", szServerMask, szRemoteServer );
		else IRCSend( "LINKS %s", szServerMask );
	}
	else IRCSend( "LINKS" );
}

void CIRC::Time( const char* szServer )
{
	if ( *szServer ) IRCSend( "TIME %s", szServer );
	else IRCSend( "TIME" );
}

void CIRC::Trace( const char* szServer )
{
	if ( *szServer ) IRCSend( "TRACE %s", szServer );
	else IRCSend( "TRACE" );
}

void CIRC::Admin( const char* szServer )
{
	if ( *szServer ) IRCSend( "ADMIN %s", szServer );
	else IRCSend( "ADMIN" );
}

void CIRC::Info( const char* szServer )
{
	if ( *szServer ) IRCSend( "INFO %s", szServer );
	else IRCSend( "INFO" );
}

void CIRC::IRCSend( const char* szText, ... )
{
	// The IRC protocol has a max message size of 512
	char sz[ 512 ] = { 0 };
	va_list marker;
	va_start( marker, szText );
	vsnprintf( sz, 509, szText, marker );
	va_end( marker );

	strcat( sz, "\r\n" );

	//printf( "Sending %s", sz );

	if ( ( m_pSocket ) && ( m_bConnected ) ) 
	{
#if _IRC_OPENSSL_SUPPORT==1
		if ( m_bUsingSSL )
		{
			if ( m_pSSLSocket )
				SSL_write( m_pSSLSocket, sz, (int)strlen( sz ) );
		}
		else
#endif
			send( m_pSocket, sz, (int)strlen( sz ), 0 );
	}
}

// The Manager
unsigned int CIRCManager::m_uiCount = 0;
unsigned int CIRCManager::m_uiLastFreeID = 0;
CIRC* CIRCManager::m_pIRCs[ _IRC_MAX_CONNS ] = { 0 };

void CIRCManager::Init( void )
{
#if defined( WIN32 ) || defined( _WIN64 )
	// We init winsock here, because this should be called before anything is used in CIRC
	WSADATA wsaData;
	if ( !WSAStartup( MAKEWORD( 2, 0 ), &wsaData ) )
	{
		assert( ( LOBYTE(wsaData.wVersion) >= 2 ) );
	}
#endif

#if _IRC_OPENSSL_SUPPORT==1
	SSL_library_init();
#endif
}

CIRC* CIRCManager::New( void )
{
	const unsigned int ui = m_uiLastFreeID;
	if ( ui < _IRC_MAX_CONNS )
	{
		CIRC* pIRC = new(std::nothrow) CIRC( ui );
		if ( pIRC )
		{
			m_uiCount++;
			m_pIRCs[ ui ] = pIRC;

			m_uiLastFreeID++;
			m_uiLastFreeID = FindFreeID();

			return pIRC;
		}
	}

	return 0;
}

CIRC* CIRCManager::New( const unsigned int ui )
{
	if ( ui < _IRC_MAX_CONNS )
	{
		CIRC* pIRC = new(std::nothrow) CIRC( ui );
		if ( pIRC )
		{
			m_uiCount++;
			m_pIRCs[ ui ] = pIRC;

			if ( ui == m_uiLastFreeID )
			{
				m_uiLastFreeID++;
				m_uiLastFreeID = FindFreeID();
			}
			else 
				m_uiLastFreeID = FindFreeID();

			return pIRC;
		}
	}

	return 0;
}

bool CIRCManager::Remove( CIRC* p )
{
	if ( p )
	{
		for ( unsigned int ui = 0; ui < _IRC_MAX_CONNS; ++ui )
		{
			if ( m_pIRCs[ ui ] == p ) 
			{
				m_uiCount--;
				m_pIRCs[ ui ] = 0;
				delete p;
				return true;
			}
		}
	}

	return false;
}

bool CIRCManager::Remove( const unsigned int ui )
{
	if ( ui < _IRC_MAX_CONNS )
	{
		if ( m_pIRCs[ ui ] ) 
		{
			m_uiCount--;
			delete m_pIRCs[ ui ];
			m_pIRCs[ ui ] = 0;
			return true;
		}
	}

	return false;
}

void CIRCManager::RemoveAll( void )
{
	for ( unsigned int ui = 0; ui < _IRC_MAX_CONNS; ++ui )
	{
		if ( m_pIRCs[ ui ] ) 
		{
			delete m_pIRCs[ ui ];
			m_pIRCs[ ui ] = 0;
		}
	}

	m_uiCount = 0;
}

unsigned int CIRCManager::FindFreeID( void )
{
	for ( unsigned int ui = m_uiLastFreeID; ui < _IRC_MAX_CONNS; ++ui )
	{
		if ( !m_pIRCs[ ui ] ) 
			return ui;
	}

	return _IRC_MAX_CONNS;
}
