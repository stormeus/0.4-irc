#include "IRCChannels.h"

CIRCChannel::CIRCChannel( CIRCServerInfo* pServerInfo, CIRCUsers* pUsers, const char* szName )
: m_pServerInfo( pServerInfo ), m_pUsers( pUsers ), m_pChannelUsers( 0 )
{
	m_szName = new char[ m_pServerInfo->GetMaxChanNameLen() ];
	irc_strcpy( m_szName, szName, m_pServerInfo->GetMaxChanNameLen() );

	m_pTopicInfo.szTopic = new char[ m_pServerInfo->GetMaxChanTopicLen() ];
	m_pTopicInfo.szNick = new char[ m_pServerInfo->GetMaxNickLen() ];
	m_pTopicInfo.uiTime = 0;

	for ( size_t ui = 0; ui < sizeof( m_cChannelModes ); ++ui )
		m_cChannelModes[ ui ] = 0;
}

CIRCChannel::~CIRCChannel( void )
{
	RemoveAllUsers();

	if ( m_szName )
	{
		delete [] m_szName;
		m_szName = 0;
	}

	if ( m_pTopicInfo.szTopic )
	{
		delete [] m_pTopicInfo.szTopic;
		m_pTopicInfo.szTopic = 0;
	}

	if ( m_pTopicInfo.szNick )
	{
		delete [] m_pTopicInfo.szNick;
		m_pTopicInfo.szNick = 0;
	}
	// DO NOT DELETE m_pServerInfo. It comes from CIRC
}

void CIRCChannel::SetChannelModes( const char *sz )
{
	const char* p = sz;
	bool bAdding = true, bUserMode = false;;

	size_t uiLen = strlen( p )+1;
	char* szTmp = (char*)alloca( uiLen + 2 );
	irc_strcpy( szTmp, p, uiLen );
	size_t uiTokens = NumTok( szTmp, ' ' ), uiTokensUsed = 0;

	if ( uiTokens > 1 )
	{
		strtok( szTmp, " " );
		irc_strcpy( szTmp, strtok( 0, " " ), uiLen );
		uiTokensUsed += 2;
	}

	for ( ; *p && *p != ' '; *p++ )
	{
		bUserMode = false;
		if ( *p == '+' )
		{
			bAdding = true;
			continue;
		}
		else if ( *p == '-' )
		{
			bAdding = false;
			continue;
		}
		else
		{
			if ( bAdding )
			{
				// DO SOME HANDLING FOR USERS!
				// Would involve checking against prefix stuff

				// Check if the first character is a prefix mode
				size_t ui = 0;
				while ( ui < _IRC_PREFIX_MODES_SIZE )
				{
					if ( *p == m_pServerInfo->GetPrefixMode( ui ).cMode )
					{
						bUserMode = true;

						char cMode = m_pServerInfo->GetPrefixMode( ui ).cMode;

						// Ok, the mode set is listed in the 'PREFIX' setion of the ISUPPORT.
						// We now need to get the nickname and find it in our list

						IRC_CHANNEL_USERS* pHook = m_pChannelUsers;

						CIRCUser* pUser = m_pUsers->Find( szTmp );

						while ( pHook )
						{
							if ( pHook->pUser == pUser )
							{
								for ( size_t ui1 = 0; ui1 < sizeof( pHook->cModes ); ui1++ )
								{
									if ( pHook->cModes[ ui1 ] == cMode )
									{
										// Ignore if its already listed
										break;
									}

									else if ( !pHook->cModes[ ui1 ] )
									{
										pHook->cModes[ ui1 ] = cMode;
										break;
									}
								}
								break;
							}
							pHook = pHook->pNext;
						}

						if ( uiTokensUsed < uiTokens )
						{
							irc_strcpy( szTmp, strtok( 0, " " ), uiLen );
							uiTokensUsed++;
						}
						
					}
					++ui;
				}

				if ( !bUserMode )
				{
					for ( size_t ui = 0; ui < sizeof( m_cChannelModes ); ++ui )
					{
						if ( !m_cChannelModes[ ui ] )
						{
							m_cChannelModes[ ui ] = *p;
							break;
						}
					}
				}
			}
			else
			{
				// Check if the first character is a prefix mode
				size_t ui = 0;
				while ( ui < _IRC_PREFIX_MODES_SIZE )
				{
					if ( *p == m_pServerInfo->GetPrefixMode( ui ).cMode )
					{
						bUserMode = true;

						char cMode = m_pServerInfo->GetPrefixMode( ui ).cMode;

						// Ok, the mode set is listed in the 'PREFIX' setion of the ISUPPORT.
						// We now need to get the nickname and find it in our list

						IRC_CHANNEL_USERS* pHook = m_pChannelUsers;

						CIRCUser* pUser = m_pUsers->Find( szTmp );

						while ( pHook )
						{
							if ( pHook->pUser == pUser )
							{
								for ( size_t ui1 = 0; ui1 < sizeof( pHook->cModes ); ++ui1 )
								{
									if ( pHook->cModes[ ui1 ] == cMode )
									{
										pHook->cModes[ ui1 ] = 0;
										for ( size_t j = ui1+1; j < sizeof( pHook->cModes ); j++ )
										{
											pHook->cModes[ j-1 ] = pHook->cModes[ j ];
										}
										break;
									}
								}
								break;
							}
							pHook = pHook->pNext;
						}

						if ( uiTokensUsed < uiTokens )
						{
							irc_strcpy( szTmp, strtok( 0, " " ), uiLen );
							uiTokensUsed++;
						}
						
					}
					++ui;
				}

				if ( !bUserMode )
				{
					for ( size_t ui = 0; ui < sizeof( m_cChannelModes ); ++ui )
					{
						if ( m_cChannelModes[ ui ] == *p )
						{
							m_cChannelModes[ ui ] = 0;
							for ( size_t j = ui+1; j < sizeof( m_cChannelModes ); ++j )
							{
								m_cChannelModes[ j-1 ] = m_cChannelModes[ j ];
							}

							break;
						}
					}
				}
			}
		}
	}
}

void CIRCChannel::AddUser( char *sz )
{
	char* p = sz;
	char cMode = 0;

	// Check if the first character is a prefix mode
	size_t ui = 0;
	while ( ui < _IRC_PREFIX_MODES_SIZE )
	{
		if ( *p == m_pServerInfo->GetPrefixMode( ui ).cPrefix )
		{
			cMode = m_pServerInfo->GetPrefixMode( ui ).cMode;
			*p++;
		}
		++ui;
	}

	CIRCUser* pUser = m_pUsers->New( p );

	if ( !m_pChannelUsers )
	{
		m_pChannelUsers = new IRC_CHANNEL_USERS;
		m_pChannelUsers->pPrev = 0;
		m_pChannelUsers->cModes[ 0 ] = cMode;

		for ( size_t ui = 1; ui < sizeof( m_pChannelUsers->cModes ); ++ui )
			m_pChannelUsers->cModes[ ui ] = 0;

		m_pChannelUsers->pUser = pUser;
		m_pChannelUsers->pNext = 0;
	}
	else
	{
		IRC_CHANNEL_USERS* pHook = m_pChannelUsers;

		while ( pHook )
		{
			if ( !pHook->pNext )
			{
				pHook->pNext = new IRC_CHANNEL_USERS;

				pHook->pNext->pPrev = pHook;

				pHook = pHook->pNext;
				pHook->cModes[ 0 ] = cMode;

				for ( size_t ui = 1; ui < sizeof( pHook->cModes ); ++ui )
					pHook->cModes[ ui ] = 0;

				pHook->pUser = pUser;
				pHook->pNext = 0;

				return;
			}
			pHook = pHook->pNext;
		}
	}
}

void CIRCChannel::RemoveUser( const char *sz )
{
	IRC_CHANNEL_USERS* pHook = m_pChannelUsers;

	CIRCUser* pUser = m_pUsers->Find( sz );

	if ( !pUser )
		return;

	while ( pHook )
	{
		if ( pHook->pUser == pUser )
		{
			if ( pHook->pPrev )
				pHook->pPrev->pNext = pHook->pNext;

			if ( pHook->pNext )
				pHook->pNext->pPrev = pHook->pPrev;

			if ( pHook == m_pChannelUsers )
				m_pChannelUsers = pHook->pNext;

			pHook->pPrev = 0;
			pHook->pNext = 0;

			m_pUsers->Remove( pUser );
			
			delete pHook;

			break;
		}
		pHook = pHook->pNext;
	}
}

void CIRCChannel::RemoveAllUsers( void )
{
	if ( m_pChannelUsers )
	{
		IRC_CHANNEL_USERS* pHook = m_pChannelUsers;
		IRC_CHANNEL_USERS* pNextHook = 0;

		while ( pHook )
		{
			pNextHook = pHook->pNext;

			pHook->pNext = 0;
			pHook->pPrev = 0;

			m_pUsers->Remove( pHook->pUser );

			delete pHook;

			pHook = pNextHook;
		}

		m_pChannelUsers = 0;
	}
}

const char* CIRCChannel::GetUserModes( const char *sz )
{
	IRC_CHANNEL_USERS* pHook = m_pChannelUsers;

	CIRCUser* pUser = m_pUsers->Find( sz );

	if ( !pUser )
		return "";

	while ( pHook )
	{
		if ( pHook->pUser == pUser )
		{
			return pHook->cModes;
		}
		pHook = pHook->pNext;
	}

	return "";
}

// The following keeps track of channels for each IRC connection
CIRCChannels::CIRCChannels( CIRCServerInfo* pServerInfo, CIRCUsers*	pUsers )
: m_uiCount( 0 ), m_pServerInfo( pServerInfo ), m_pUsers( pUsers )
{
	m_pChannels.clear();
}

CIRCChannels::~CIRCChannels(void)
{
	RemoveAll();

	// DO NOT DELETE m_pServerInfo. It comes from CIRC
}

CIRCChannel* CIRCChannels::New( const char* szName )
{
	if ( *szName )
	{
		CIRCChannel* pChan = new(std::nothrow) CIRCChannel( m_pServerInfo, m_pUsers, szName );
		if ( pChan )
		{
			m_pChannels[ std::string( szName ) ] = pChan;
			m_uiCount++;

			return pChan;
		}
	}

	return 0;
}

bool CIRCChannels::Remove( CIRCChannel* p )
{
	if ( p )
	{
		m_pChannels.erase( std::string( p->GetName() ) );
		m_uiCount--;

		delete p;

		return true;
	}

	return false;
}

bool CIRCChannels::Remove( const char* szName )
{
	CIRCChannel* p = Find( szName );
	if ( p )
	{
		m_pChannels.erase( std::string( szName ) );
		m_uiCount--;

		delete p;

		return true;
	}

	return false;
}

void CIRCChannels::RemoveAll( void )
{
	stdext::hash_map< std::string, CIRCChannel* >::const_iterator iter = m_pChannels.begin();
	stdext::hash_map< std::string, CIRCChannel* >::const_iterator iter_end = m_pChannels.end();
	
	for( ; iter != iter_end; ++iter )
	{
		delete (*iter).second;
	}

	m_uiCount = 0;
	m_pChannels.clear();
}
