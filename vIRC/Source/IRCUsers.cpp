#include "IRCUsers.h"

CIRCUser::CIRCUser( CIRCServerInfo* pServerInfo, const char* szName )
: m_pServerInfo( pServerInfo )
{
	m_szName = new char[ m_pServerInfo->GetMaxNickLen() ];
	irc_strcpy( m_szName, szName, m_pServerInfo->GetMaxNickLen() );
}

CIRCUser::~CIRCUser( void )
{
	if ( m_szName )
	{
		delete [] m_szName;
		m_szName = 0;
	}

	// DO NOT DELETE m_pServerInfo. It comes from CIRC
}

CIRCUsers::CIRCUsers( CIRCServerInfo* pServerInfo )
: m_pServerInfo( pServerInfo ), m_uiCount( 0 )
{
	m_pUsers.clear();
}

CIRCUsers::~CIRCUsers(void)
{
	// DO NOT DELETE m_pServerInfo. It comes from CIRC
	RemoveAll();
}

CIRCUser* CIRCUsers::New( const char* szName )
{
	if ( *szName )
	{
		CIRCUser* pUser = Find( szName );
		if ( pUser )
		{
			pUser->Grab();
			return pUser;
		}
		else 
		{
			pUser = new(std::nothrow) CIRCUser( m_pServerInfo, szName );
			if ( pUser )
			{
				m_pUsers[ std::string( szName ) ] = pUser;
				m_uiCount++;

				return pUser;
			}
		}
	}

	return 0;
}

bool CIRCUsers::Remove( CIRCUser* p )
{
	if ( p )
	{
		std::string s = std::string( p->GetName() );
		if ( p->Drop() )
		{
			m_pUsers.erase( s );
			m_uiCount--;

			return true;
		}
	}

	return false;
}

bool CIRCUsers::Remove( const char* szName )
{
	CIRCUser* p = Find( szName );
	if ( p )
	{
		if ( p->Drop() )
		{
			m_pUsers.erase( std::string( szName ) );
			m_uiCount--;

			return true;
		}
	}

	return false;
}

void CIRCUsers::RemoveAll( void )
{
	stdext::hash_map< std::string, CIRCUser* >::const_iterator iter = m_pUsers.begin();
	stdext::hash_map< std::string, CIRCUser* >::const_iterator iter_end = m_pUsers.end();
	
	for( ; iter != iter_end; ++iter )
	{
		(*iter).second->Drop();
	}

	m_uiCount = 0;
	m_pUsers.clear();
}
