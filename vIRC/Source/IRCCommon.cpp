#include "IRCCommon.h"

bool irc_isequal( const char* s1, const char* s2 )
{
	while ( *s1 == *s2++ )
	{
		if ( !*s1++ ) return true;
	}

	return false;
}

void irc_strcpy( char* szDest, const char* szSource, size_t size )
{
	while ( ( --size ) && ( *szSource ) )
	{
		*szDest++ = *szSource++;
	}

	*szDest = '\0';
}

void irc_strcat( char* szDest, const char* szSource, size_t size )
{
	while ( ( --size ) && ( *szDest ) )
	{
		*szDest++;
	}

	while ( ( --size ) && ( *szSource ) )
	{
		*szDest++ = *szSource++;
	}

	*szDest = '\0';
}

size_t NumTok( const char* szText, const char cDelimiter )
{
	if ( !*szText ) return 0;

	size_t iCounter = 1;
	char* p = (char*)szText;

	if ( *p == cDelimiter ) 
		iCounter = 0;

	while ( *p )
	{
		if ( *p == cDelimiter )
		{
			if ( ( *++p ) && ( *p != cDelimiter ) ) iCounter++;
		}
		else *p++;
	}

	return iCounter;
}

bool IsNum( const char* sz )
{
	register const char *s=sz;

	if (!*s) return false;
	for(;;)
	{
		if (!*s) break; if ( *s < '0' || *s > '9' ) return false; ++s;
		if (!*s) break; if ( *s < '0' || *s > '9' ) return false; ++s;
		if (!*s) break; if ( *s < '0' || *s > '9' ) return false; ++s;
		if (!*s) break; if ( *s < '0' || *s > '9' ) return false; ++s;
	}
	
	return true;
}
