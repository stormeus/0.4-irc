#pragma once
#ifndef _IRC_CONFIG
#define _IRC_CONFIG

// Compile with OpenSSL support
#ifndef _IRC_OPENSSL_SUPPORT

#ifdef _WIN32_WCE
	// OpenSSL is only supported on x86 atm so dont change this!
	#define _IRC_OPENSSL_SUPPORT 0
#else
	#define _IRC_OPENSSL_SUPPORT 1
#endif

#endif

// Maximum IRC connections that can be made
#ifndef _IRC_MAX_CONNS
	#define _IRC_MAX_CONNS 25
#endif

#endif
