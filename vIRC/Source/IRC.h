#pragma once
#ifndef _IRC_H
#define _IRC_H

// If we're compiling for windows, we need to include these files
#if defined( _WIN32 ) || defined( _WIN64 )
	#include <winsock2.h>
	#include <windows.h>
	#include <ws2tcpip.h>

	#define STDCALL __stdcall
#else
	#include <sys/types.h> 
	#include <sys/socket.h>
	#include <netdb.h>
	#include <sys/ioctl.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <stdarg.h>
	#include <unistd.h>

	#define STDCALL

	#ifndef INVALID_SOCKET
	#define INVALID_SOCKET -1
	#endif

	#ifndef SOCKET_ERROR
	#define SOCKET_ERROR -1
	#endif

	typedef unsigned int SOCKET;
#endif

#include <time.h>

#include "IRCConfig.h"
#include "IRCServerInfo.h"
#include "IRCChannels.h"
#include "IRCUsers.h"

#if _IRC_OPENSSL_SUPPORT==1
//#include <openssl/bio.h> // BIO objects for I/O
#include <openssl/crypto.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/ssl.h> // SSL and SSL_CTX for SSL connections
#include <openssl/err.h> // Error reporting
#endif

typedef int (STDCALL *IRCCmdCallback)( const char* szHost, const char* szData, void* pIRC );
typedef int (STDCALL *IRCNumAllCmdCallback)( unsigned int uiNumeric, const char* szHost, const char* szData, void* pIRC );
typedef int (STDCALL *IRCDisconnectCallback)( void* pIRC );

enum _IRC_CONNECTION_RETURNS
{
	IRC_CONN_SUCCESSFUL,
	IRC_CONN_FAILED,
	IRC_CONN_ALREADYCONNECTED,
	IRC_CONN_NOIP,
	IRC_CONN_NOPORT,
	IRC_CONN_NONICK,
	IRC_CONN_NOUSER,
	IRC_CONN_NOREALNAME,
};

enum _IRC_SERVER_REPLIES
{
	RPL_ALL = 000, // This has been added so we can have a callback for all numerics
	// Command Responses
	RPL_WELCOME = 001,
	RPL_YOURHOST = 002,
	RPL_CREATED = 003,
	RPL_MYINFO = 004,
	RPL_ISUPPORT = 005,
	RPL_TRACELINK = 200,
	RPL_TRACECONNECTING = 201,
	RPL_TRACEHANDSHAKE = 202,
	RPL_TRACEUNKNOWN = 203,
	RPL_TRACEOPERATOR = 204,
	RPL_TRACEUSER = 205,
	RPL_TRACESERVER = 206,
	RPL_TRACENEWTYPE = 208,
	RPL_STATSLINKINFO = 211,
	RPL_STATSCOMMANDS = 212,
	RPL_STATSCLINE = 213,
	RPL_STATSNLINE = 214,
	RPL_STATSILINE = 215,
	RPL_STATSKLINE = 216,
	RPL_STATSYLINE = 218,
	RPL_RPLENDOFSTATS = 219,
	RPL_UMODEIS = 221,
	RPL_SERVICEINFO = 231,
	RPL_ENDOFSERVICES = 232,
	RPL_SERVICE = 233,
	RPL_STATSLLINE = 241,
	RPL_STATSUPTIME = 242,
	RPL_STATSOLINE = 243,
	RPL_STATSHLINE = 244,
	RPL_LUSERCLIENT = 251,
	RPL_LUSEROP = 252,
	RPL_LUSERUNKNOWN = 253,
	RPL_LUSERCHANNELS = 254,
	RPL_LUSERME = 255,
	RPL_ADMINME = 256,
	RPL_ADMINLOC1 = 257,
	RPL_ADMINLOC2 = 258,
	RPL_ADMINEMAIL = 259,
	RPL_TRACELOG = 261,
	RPL_NONE = 300,
	RPL_AWAY = 301,
	RPL_USERHOST = 302,
	RPL_ISON = 303,
	RPL_UNAWAY = 305,
	RPL_NOWAWAY = 306,
	RPL_WHOISUSER = 311,
	RPL_WHOISSERVER = 312,
	RPL_WHOISOPERATOR = 313,
	RPL_WHOWASUSER = 314,
	RPL_ENDOFWHO = 315,
	RPL_WHOISCHANOP = 316,
	RPL_WHOISIDLE = 317,
	RPL_ENDOFWHOIS = 318,
	RPL_WHOISCHANNELS = 319,
	RPL_LISTSTART = 321,
	RPL_LIST = 322,
	RPL_LISTEND = 323,
	RPL_CHANNELMODEIS = 324,
	RPL_CREATIONTIME = 329,
	RPL_NOTOPIC = 331,
	RPL_TOPIC = 332,
	RPL_TOPICINFO = 333,
	RPL_INVITING = 341,
	RPL_SUMMONING = 342,
	RPL_VERSION = 351,
	RPL_WHOREPLY = 352,
	RPL_NAMREPLY = 353,
	RPL_KILLDONE = 361,
	RPL_CLOSING = 362,
	RPL_CLOSEEND = 363,
	RPL_LINKS = 364,
	RPL_ENDOFLINKS = 365,
	RPL_ENDOFNAMES = 366,
	RPL_BANLIST = 367,
	RPL_ENDOFBANLIST = 368,
	RPL_ENDOFWHOWAS = 369,
	RPL_INFO = 371,
	RPL_MOTD = 372,
	RPL_INFOSTART = 373,
	RPL_ENDOFINFO = 374,
	RPL_MOTDSTART = 375,
	RPL_ENDOFMOTD = 376,
	RPL_YOUREOPER = 381,
	RPL_REHASHING = 382,
	RPL_MYPORTIS = 384,
	RPL_TIME = 391,
	RPL_USERSSTART = 392,
	RPL_USERS = 393,
	RPL_ENDOFUSERS = 394,
	RPL_NOUSERS = 395,

	// Error Responses
	ERR_NOSUCHNICK = 401,
	ERR_NOSUCHSERVER = 402,
	ERR_NOSUCHCHANNEL = 403,
	ERR_CANNOTSENDTOCHAN = 404,
	ERR_TOOMANACHANNELS = 405,
	ERR_WASNOSUCHNICK = 406,
	ERR_TOOMANYTARGETS = 407,
	ERR_NOORIGIN = 409,
	ERR_NORECIPIENT = 411,
	ERR_NOTEXTTOSEND = 412,
	ERR_NOTOPLEVEL = 413,
	ERR_WILDTOPLEVEL = 414,
	ERR_UNKNOWNCOMMAND = 421,
	ERR_NOMOTD = 422,
	ERR_NOADMININFO = 423,
	ERR_FILEERROR = 424,
	ERR_NONICKNAMEGIVEN = 431,
	ERR_ERRONEUSNICKNAME = 432,
	ERR_NICKNAMEINUSE = 433,
	ERR_NICKCOLLISION = 436,
	ERR_USERNOTINCHANNEL = 441,
	ERR_NOTONCHANNEL = 442,
	ERR_USERONCHANNEL = 443,
	ERR_NOLOGIN = 444,
	ERR_SUMMONDISABLED = 445,
	ERR_USERSDISABLED = 446,
	ERR_NOTREGISTERED = 451,
	ERR_NEEDMOREPARAMS = 461,
	ERR_ALREADYREGISTERED = 462,
	ERR_NOPERMFORHOST = 463,
	ERR_PASSWDMISMATCH = 464,
	ERR_YOUREBANNEDCREEP = 465,
	ERR_KEYSET = 467,
	ERR_CHANNELISFULL = 471,
	ERR_UNKNOWNMODE = 472,
	ERR_INVITEONLYCHAN = 473,
	ERR_BANNEDFROMCHAN = 474,
	ERR_BADCHANNELKEY = 475,
	ERR_NOPRIVILEGES = 481,
	ERR_CHANOPRIVSNEEDED = 482,
	ERR_CANTKILLSERVER = 483,
	ERR_NOOPERHOST = 491,
	ERR_NOSERVICEHOST = 492,
	ERR_UMODEUNKNOWNFLAG = 501,
	ERR_USERSDONTMATCH = 502,

	_IRC_SERVER_REPLIES_SIZE
};

struct IRCCommandHook
{
	IRCCommandHook* pPrev;
	char* szIRCCmd;
	void* pCallback;
	IRCCommandHook* pNext;
};

struct IRCNumericCommandHook
{
	IRCNumericCommandHook* pPrev;
	void* pCallback;
	IRCNumericCommandHook* pNext;
};

class CIRC
{
public:
	CIRC( unsigned char uc );
	~CIRC(void);

	///<summary>Sets the IP for the client to connect to</summary>
	///<param name="sz">The ip to connect to</param>
	void								SetIP					( char* sz )					{ irc_strcpy( m_szIP, sz, sizeof( m_szIP ) ); }
	///<returns>Returns the IP the client is connecting to</returns>
	const char*							GetIP					( void ) const					{ return m_szIP; }

	///<summary>Sets the port of the server that the client will connect to</summary>
	///<param name="us">The port to connect to</param>
	void								SetPort					( unsigned short us )			{ m_usPort = us; }
	///<returns>Returns the port of the server the client is connecting to</returns>
	unsigned short						GetPort					( void ) const					{ return m_usPort; }

	///<summary>Sets the password to connect to the server with</summary>
	///<param name="sz">The password to use</param>
	void								SetPass					( const char* sz )				{ irc_strcpy( m_szPass, sz, sizeof( m_szPass ) ); }
	///<returns>Returns the password the client is using</returns>
	const char*							GetPass					( void ) const					{ return m_szPass; }

	///<summary>Sets the nickname to connect to the server as</summary>
	///<param name="sz">The nickname to use</param>
	void								SetNick					( const char* sz );
	///<returns>Returns the nickname the client is using</returns>
	const char*							GetNick					( void ) const					{ return m_szNick; }

	///<summary>Sets the username to connect to the server as</summary>
	///<param name="sz">The username to use</param>
	void								SetUsername				( const char* sz )				{ irc_strcpy( m_szUsername, sz, sizeof( m_szUsername ) ); }
	///<returns>Returns the username the client is using</returns>
	const char*							GetUsername				( void ) const					{ return m_szUsername; }

	///<summary>Sets the realname to connect to the server as</summary>
	///<param name="sz">The realname to use</param>
	void								SetRealname				( const char* sz )				{ irc_strcpy( m_szRealname, sz, sizeof( m_szRealname ) ); }
	///<returns>Returns the realname the client is using</returns>
	const char*							GetRealname				( void ) const					{ return m_szRealname; }

	void								SetVersionReply			( const char* sz );

	CIRCServerInfo*						GetServerInfo			( void ) const					{ return m_pServerInfo; }
	CIRCChannels*						GetChannels				( void )						{ return m_pChannels; }
	CIRCUsers*							GetUsers				( void ) const					{ return m_pUsers; }

	///<returns>Returns wether or not the client is connected to the irc server</returns>
	bool								IsConnected				( void ) const					{ return m_bConnected; }

	///<returns>Returns the socket thats currently being used</returns>
	SOCKET								GetSocket				( void ) const					{ return m_pSocket; }

	///<summary>Set the socket in non blocking or blocking mode</summary>
	void								SetBlocking				( bool b );

	bool								GetJoinOnInvite			( void ) const					{ return m_bJoinOnInvite; }
	void								SetJoinOnInvite			( bool b )						{ m_bJoinOnInvite = b; }

	bool								SupportsSSL				( void ) const
	{
#if _IRC_OPENSSL_SUPPORT==1
		return true;
#else
		return false;
#endif
	}

	bool								GetUsingSSL				( void ) const					{ return m_bUsingSSL; }
	void								SetUsingSSL				( bool b )						
	{ 
#if _IRC_OPENSSL_SUPPORT==1
		m_bUsingSSL = b; 
#else
		m_bUsingSSL = false;
#endif
	}

	void								SetDisconnectCallback	( void* p )						{ m_pDisconnectCallback = p; }
	void								SetRawCallback			( void* p )						{ m_pRawCallback = p; }

	void								AddIRCCmdHook			( const char* szCmd, void* pCallback );
	void								DelIRCCmdHook			( IRCCommandHook* pHook );
	void								DelAllIRCCmdHooks		( void );
	void								CallIRCCmdHook			( const char* szCmd, const char* szHost, const char* szData );

	void								AddNumericIRCCmdHook	( const unsigned int uiNumeric, void* pCallback );
	void								DelNumericIRCCmdHook	( IRCNumericCommandHook* pHook );
	void								DelAllNumericIRCCmdHooks( void );
	void								CallNumericIRCCmdHook	( const unsigned int uiNumeric, const char* szHost, const char* szData );

	// Below we will create some functions to actually do stuff

	///<summary>Connects to a chosen server</summary>
	///<returns>Whether or not the connection failed, and the reason id for why</returns>
	_IRC_CONNECTION_RETURNS				Connect					( void );
	///<summary>Disconnects from the server</summary>
	void								Disconnect				( void );

	///<summary>Listens for incoming data</summary>
	unsigned int						Listen					( void );

	///<summary>Joins a specified channel</summary>
	///<param name="szChannel">Channel to join</param>
	///<param name="szPass">password to use for join</param>
	void								Join					( const char* szChannel, const char* szPass = "" );

	///<summary>Leaves a specified channel</summary>
	///<param name="szChannel">Channel to leave</param>
	///<param name="szReason">Reason for leaving</param>
	void								Part					( const char* szChannel, const char* szReason = "" );

	///<summary>Retrieves the topic of a channel</summary>
	///<param name="szChannel">Channel to get the topic of</param>
	void								Topic					( const char* szChannel );

	///<summary>Sets the topic of a channel</summary>
	///<param name="szChannel">Channel to set the topic of</param>
	///<param name="szText">What to set the topic to</param>
	void								Topic					( const char* szChannel, const char* szText );

	///<summary>Sends a privmsg to the server</summary>
	///<param name="szWho">Who to send the privmsg to. Can be a channel name</param>
	///<param name="szText">The text to send</param>
	void								PrivMsg					( const char* szWho, const char* szText );

	///<summary>Sends an action privmsg to the server</summary>
	///<param name="szWho">Who to send the action to. Can be a channel name</param>
	///<param name="szText">The text to send</param>
	void								Action					( const char* szWho, const char* szText );

	///<summary>Sends a notice to the server</summary>
	///<param name="szWho">Who to send the notice to. Can be a channel name</param>
	///<param name="szText">The text to send</param>
	void								Notice					( const char* szWho, const char* szText );

	///<summary>Sends an oper login attempt</summary>
	///<param name="szUser">The username to login with</param>
	///<param name="szPass">The password to use for login</param>
	void								Oper					( const char* szUser, const char* szPass );

	///<summary>Sets the mode of a user</summary>
	///<param name="szUser">The username to set the mode of</param>
	///<param name="szMode">The mode to set</param>
	void								Mode					( const char* szUser, const char* szMode );

	///<summary>Sends a QUIT message to the server</summary>
	///<param name="szReason">Reason for the quit</param>
	void								Quit					( const char* szReason );

	///<summary>Sends a NAMES message to the server</summary>
	///<param name="szChannel">The channel to list the names of</param>
	void								Names					( const char* szChannel = 0 );

	///<summary>Sends a LIST message to the server</summary>
	///<param name="szChannel">The channels to list the topics of</param>
	void								List					( const char* szChannel = 0 );

	///<summary>Invites a user to the channel</summary>
	///<param name="szUser">The username to invite</param>
	///<param name="szChannel">The channel to invite to</param>
	void								Invite					( const char* szUser, const char* szChannel );

	///<summary>Kicks a user from the channel</summary>
	///<param name="szChannel">Channel to kick the user from</param>
	///<param name="szUser">User to kick</param>
	///<param name="szReason">Reason for kicking the user</param>
	void								Kick					( const char* szChannel, const char* szUser, const char* szReason = 0 );

	// Server Queries and Commands
	///<summary>Queries the version of the IRCd</summary>
	///<param name="szServer">The server to query</param>
	void								Version					( const char* szServer = 0 );

	///<summary>Queries the statistics of a server</summary>
	///<param name="szQuery">The query</param>
	///<param name="szServer">The server to query</param>
	void								Stats					( const char* szQuery, const char* szServer = 0 );

	///<summary>List all the links known by the server</summary>
	///<param name="szServerMask">The mask to match the server list by</param>
	///<param name="szRemoteServer">Where to forward the LINKS command to</param>
	void								Links					( const char* szServerMask = 0, const char* szRemoteServer = 0 );

	///<summary>Query the local time from the server</summary>
	///<param name="szServer">The server to query the time from</param>
	void								Time					( const char* szServer = 0 );

	///<summary>Finds the route to a specific server</summary>
	///<param name="szServer">The server to trace to</summary>
	void								Trace					( const char* szServer = 0 );

	///<summary>Returns the name of the admin for the server</summary>
	///<param name="szServer">The server to query</param>
	void								Admin					( const char* szServer = 0 );

	///<summary>Retrieves information about the server</summary>
	///<param name="szServer">The server to query</param>
	void								Info					( const char* szServer = 0 );

	///<summary>Sends raw data to the irc server</summary>
	///<param name="sz">The raw text to send</param>
	void								IRCSend					( const char* sz, ... );

private:
	void								ParseIRCReplies			( char* szBuffer );

	unsigned char						m_ucID;

	char								m_szIP[ 128 ];
	unsigned short						m_usPort;

	char								m_szPass[ 32 ];
	char								m_szNick[ 32 ];
	char								m_szUsername[ 32 ];
	char								m_szRealname[ 64 ];

	char*								m_szVersionReply;

	bool								m_bConnected;

	bool								m_bJoinOnInvite;

	bool								m_bUsingSSL;

	SOCKET								m_pSocket;

	CIRCServerInfo*						m_pServerInfo;
	CIRCChannels*						m_pChannels;
	CIRCUsers*							m_pUsers;

	IRCCommandHook*						m_pCmdHooks;
	IRCNumericCommandHook*				m_pNumericCmdHooks[ _IRC_SERVER_REPLIES_SIZE ];

	void*								m_pDisconnectCallback;
	void*								m_pRawCallback;

#if _IRC_OPENSSL_SUPPORT==1
	SSL_CTX*							m_pSSLContext;
	SSL*								m_pSSLSocket;
#endif
};

class CIRCManager
{
public:
	static void							Init					( void );

	static CIRC*						New						( void );
	static CIRC*						New						( const unsigned int ui );

	static CIRC*						Find					( const unsigned int ui )
	{
		if ( ui < _IRC_MAX_CONNS ) return m_pIRCs[ ui ];

		return 0;
	}

	static bool							Remove					( CIRC* p );
	static bool							Remove					( const unsigned int ui );

	static void							RemoveAll				( void );

	static unsigned int					FindFreeID				( void );

	static unsigned int					Count					( void )						{ return m_uiCount; }

private:
	static unsigned int					m_uiCount;
	static unsigned int					m_uiLastFreeID;
	static CIRC*						m_pIRCs[ _IRC_MAX_CONNS ];
};

#endif

