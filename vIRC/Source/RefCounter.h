#pragma once

class IRefCounter
{
public:
	IRefCounter()
		: m_iCounter( 1 )
	{

	}

	virtual ~IRefCounter() 
	{

	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>	Grabs this object. Incriments the reference counter by 1. </summary>
	///
	/// <remarks>	If you call Grab, you must also call 'Drop' when you no longer need it. </remarks>
	////////////////////////////////////////////////////////////////////////////////////////////////////
	void							Grab						( void ) const						{ ++m_iCounter; }

	bool							Drop						( void ) const
	{
		--m_iCounter;
		if ( !m_iCounter )
		{
			delete this;
			return true;
		}

		return false;
	}

	unsigned int					GetRefCount					( void ) const						{ return m_iCounter; }

private:
	mutable unsigned int			m_iCounter;
};
