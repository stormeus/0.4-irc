#include "../../Source/IRC.h"

CIRC* g_pIRC = 0;
bool g_bRunning = true;

int __stdcall RawOutput( const char* szHost, const char* szData, CIRC* pIRC )
{
	//printf( "Host: %s, Data: %s\n", szHost, szData );
	return 1;
}

int __stdcall PrivMsg( const char* szHost, const char* szData, CIRC* pIRC )
{
	//printf( "Host: %s, Data: %s\n", szHost, szData );

	CIRCServerInfo*	pServerInfo = pIRC->GetServerInfo();

	char szTmp[ 512 ] = { 0 };
	strcpy( szTmp, szHost );
	char* szNick = (char*)alloca( pServerInfo->GetMaxNickLen() );
	irc_strcpy( szNick, strtok( szTmp, "!" ) + 1, pServerInfo->GetMaxNickLen() );

	strcpy( szTmp, szData );
	char* szChannel = (char*)alloca( pServerInfo->GetMaxChanNameLen() );
	irc_strcpy( szChannel, strtok( szTmp, " " ), pServerInfo->GetMaxChanNameLen() );

	// Max message length is 512
	char szMessage[ 510 ] = { 0 };
	irc_strcpy( szMessage, szData + strlen( szChannel ) + 1 + 1, sizeof( szMessage ) );

	CIRCChannel* pChan = pIRC->GetChannels()->Find( szChannel );
	if ( pChan )
	{
		const char* szModes = pChan->GetUserModes( szNick );
		//printf( "Modes: %s\n", szModes );
	}
	printf( "(%s) %s: %s\n", szChannel, szNick, szMessage );

	return 1;
}

int __stdcall ISupport( const char* szHost, const char* szData, CIRC* pIRC )
{
	printf( "SUPPORT! Host: %s, Data: %s\n", szHost, szData );
	return 1;
}

int __stdcall JoinChans( const char* szHost, const char* szData, CIRC* pIRC )
{
	pIRC->Join( "#testing" );
	//pIRC->Join( "#irc.test" );
	return 1;
}

int __stdcall Disconnected( CIRC* pIRC )
{
	printf( "Disconnected from IRC network\n" );

	printf( "Attempting to reconnect...\n" );
	if ( pIRC->Connect() == IRC_CONN_SUCCESSFUL )
	{
		printf( "Connected!\n" );
	}
	else 
		printf( "Unable to reconnect\n" );

	return 1;
}

void DoCleanup()
{
	g_bRunning = false;

	g_pIRC->Disconnect();

	Sleep( 5 );

	CIRCManager::RemoveAll();

	g_pIRC = 0;

	Sleep( 5 );
}

#ifdef _WIN32
BOOL CtrlHandler( DWORD fdwCtrlType ) 
{ 
  switch( fdwCtrlType ) 
  { 
    // CTRL-CLOSE: confirm that the user wants to exit. 
    case CTRL_CLOSE_EVENT: 
	  exit(0);
      return( TRUE ); 
  
    case CTRL_LOGOFF_EVENT: 
	  exit(0);
      return TRUE; 
 
    default: 
      return FALSE; 
  } 
} 
#endif

int main()
{
	printf( "Connecting to IRC...\n" );

#ifdef _WIN32
	SetConsoleCtrlHandler( (PHANDLER_ROUTINE) CtrlHandler, TRUE );
#endif

	atexit( DoCleanup );

	CIRCManager::Init();

	g_pIRC = CIRCManager::New();
	if ( g_pIRC )
	{
		//pIRC->SetIP( "91.121.221.60" );
		//g_pIRC->SetIP( "sentinel.vrocker-hosting.co.uk" );
		g_pIRC->SetIP( "irc.liberty-unleashed.co.uk" );
		g_pIRC->SetPort( 6697 );
		g_pIRC->SetNick( "Bob" );
		g_pIRC->SetJoinOnInvite( true );
		g_pIRC->SetRawCallback( RawOutput );
		g_pIRC->SetDisconnectCallback( Disconnected );
		g_pIRC->AddIRCCmdHook( "PRIVMSG", PrivMsg );
		g_pIRC->AddNumericIRCCmdHook( RPL_WELCOME, JoinChans );
		g_pIRC->AddNumericIRCCmdHook( RPL_ISUPPORT, ISupport );

		g_pIRC->SetVersionReply( "Boob Client v1.0" );

		g_pIRC->SetUsingSSL( true );

		printf( "Starting connection\n" );
		if ( g_pIRC->Connect() == IRC_CONN_SUCCESSFUL )
		{
			printf( "Connected!\n" );
			while ( g_bRunning )
			{
				unsigned int ui = g_pIRC->Listen();
				if ( ui )
				{
					//printf( "Host: %s, Data: %s\n", pPacket->szHost, pPacket->szData );
				}

				Sleep( 5 );
			}

			g_pIRC->Disconnect();
		}
	}

	printf( "Something failed..." );
}